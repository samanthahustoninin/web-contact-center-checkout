#!/bin/bash

if [ -z "$1" ]; then
  echo "Argument missing to build.sh. Please specify a build number"
  echo "Usage: build.sh <buildNumber>"
  exit 1
fi
VERSION=$1

# ==============================================================================
# Variables
# ==============================================================================

# Path Configuration
SCRIPTS_DIR="$( dirname ${BASH_SOURCE[0]} )"
PROJECT_ROOT_DIR="$( dirname "${SCRIPTS_DIR}" )"

# Dependency Versions (Used by downloaded scripts)
RUBY_VERSION=1.9.3-p327
NODE_VERSION=0.8.14

# ==============================================================================
# Main Logic
# ==============================================================================

# Set the state of the script to be the project's root
cd "$PROJECT_ROOT_DIR"
echo "Root Project Directory = $PROJECT_ROOT_DIR"
echo "Current Directory = `pwd`"
export PATH="`pwd`/node_modules/.bin":$JAVA_HOME:$PATH
echo "PATH = $PATH"

#Move to client app folder and set fetch dependencies
export npm_config_registry='http://npm.infrastructure.inintca.com/'
export npm_config_strict-ssl='false'
npm install grunt-cli
npm install

#Build the client app
echo "Running Grunt..."
grunt --no-color --component-version=$VERSION --cdn-url=$CDN_URL
