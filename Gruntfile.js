module.exports = function (grunt) {

    'use strict';

    // Set Grunt Package Settings

    var packageInfo = require('./package.json');

    // Load Custom User Development Options

    var options;

    try{
        options = require('./devEnvironment.json');
    } catch (e) {
        options = {
            coreProjectPath: '../web-contact-center'
        };
    }

    // Set Global Grunt Options

    grunt.option('component-path', __dirname);
    grunt.option('component-name', packageInfo.name.replace(/^web-contact-center-/, ''));
    grunt.option('core-path', options.coreProjectPath);

    // Load grunts tasks from package.json

    require('load-grunt-tasks')(grunt);

    // Load Web Contact Center Grunt Module

    var contactCenterModules = require('./node_modules/web-contact-center-grunt-modules/defaultGruntConfig.js');

    // Time how long tasks take.

    require('time-grunt')(grunt);

    // Grunt Configuration

    var defaultConfig = contactCenterModules.defaultConfig;
    var defaultTasks = contactCenterModules.defaultTasks;

    grunt.initConfig(defaultConfig);

    // Register Custom Tasks

    for(var key in defaultTasks){
        grunt.registerTask(key, defaultTasks[key]);
    }

};
