# README #

### web-contact-center-checkout ###

PureCloud Checkout - The best project ever.

### Getting Started ###

Clone the repo

`git clone git@bitbucket.org:inindca/web-contact-center-checkout.git`

Install dependencies, setup git hooks, etc.

`cd web-contact-center-checkout`

`npm install`

`grunt setup`

`grunt build`

Set up environment config (optional):

create `devEnvironment.json` and add entries for any of:

 * `coreProjectPath` - path to a checkout of the core project for use with unit tests


### Available Grunt Tasks ###

`grunt setup`

Performs one-time project setup tasks, such as configuring git hooks.

`grunt jshint`

Lints js files based on rules in .jshintrc files within the project.

`grunt test`

Runs unit tests. Unit tests require a reference to the core project, if none has been configured, it will check out a local copy from master under the `core-test` directory

`grunt check`

Runs jshint checks & unit tests (`grunt jshint` & `grunt test`)

`grunt build`

Builds deployment artifacts for the feature component in the `dist` directory

`grunt clean`

Removes all build & test artifact files

`grunt server`

Starts a test server that can be used to run unit tests in the browser at http://localhost:9001/core/js/test.  You can override the port with --port=1234.

`grunt watch`

Watches for changes to source files and runs linting & build.


### Running Locally ###

To run the component locally in the context of the contact-center application, see the instructions in the [contact-center project wiki](https://bitbucket.org/inindca/pure-cloud-browser-app/wiki/Running%20Feature%20Components%20Locally)

### Notes on Testing ###

The testing process needs a reference to a copy of the core application to resolve core depedencies when loading modules. The build process will resolve that reference with the following process:

* If an explicit path to the local copy of the core application defined in `devEnvironment.json`, use that.
* If no explicit path is set, look for `web-contact-center` as a sibling to the project
* If nothing is found at the configured or implicit location, check out a fresh copy of the application in the project directory under `core-test`



