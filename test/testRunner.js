(function () {
    'use strict';

    function getQueryStringValue(key) {
        key = key.replace(/[*+?\^$.\[\]{}()|\\\/]/g, '\\$&'); // escape RegEx meta chars
        var match = location.search.match(new RegExp('[?&]' + key + '=([^&]+)(&|$)'));
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }

    // ===================================================================================
    // Load Blanket.js
    // ===================================================================================

    var shouldRunCodeCoverage = window.location.href.indexOf('coverage=true') > -1;

    if (shouldRunCodeCoverage) {
      var blanketScript = document.createElement('script');
      blanketScript.src = 'lib/blanket_jasmine.js';
      blanketScript.type = 'text/javascript';
      blanketScript.async = false;

      // Configure Blanket.js
      blanketScript.setAttribute('data-cover-only', '../');
      blanketScript.setAttribute('data-cover-never', '[../lib, ../test, lib, ../infoArchitecture]');
      blanketScript.setAttribute('data-cover-flags', 'branchTracking');

      // In order for Blanket.js to pick up the configuration flags, it needs to be added
      // as the last script tag in the page (as of 2/20/14).
      var body = document.getElementsByTagName('body')[0];
      body.appendChild(blanketScript);
    }

    // ===================================================================================
    // Load Unit Tests
    // ===================================================================================
    var globalRequire = require;
    var pkgPath = getQueryStringValue('pkgPath') || '/root/package.json';

    /*
     * Inital require call to configure RequireJS environment for test.
     * Paths for this first call are relative to the current page, since
     * path config has not been loaded.
     */
    require([
      'specProfiles',
      'lib/URIUtil',
      'lib/text!' + pkgPath,
      '/core/js/requireConfig.js'
    ],
    function (specProfiles, URIUtil, pkgTxt) {
        // This is a "semi-private" API according to James Burke [1]
        // so we should probably find a better way to handle this,
        // but that will require changing the core application's
        // requirejs bootstrap process.
        //
        // [1]: https://groups.google.com/forum/#!topic/requirejs/Hf-qNmM0ceI
        var config = window.requirejs.s.contexts._.config;

        var pkg = JSON.parse(pkgTxt);
        var componentName = pkg.name.replace(/^web-contact-center-/, '');

        config.baseUrl = getQueryStringValue('baseUrl') || '/core/js';
        config.paths.specs = 'test/specs';
        config.paths.mocks = 'test/mocks';
        config.paths.orgspan = 'test/mocks/mockOrgSpan';

        config.paths[componentName] = getQueryStringValue('component') || '/component/js';
        console.log('RequireJS config:\n', JSON.stringify(config, null, 2));

      /*
       * Configure and style the code coverage toggle link at the top of the page
       */
      var codeCoverageLink = $('.pc-coverage-link');
      if (shouldRunCodeCoverage) {
        codeCoverageLink
          .attr('href', URIUtil.updateQueryString(window.location.href, 'coverage', 'false'))
          .addClass('pc-remove-coverage-link')
          .text('Run Tests WITHOUT Coverage');
      } else {
        codeCoverageLink
          .attr('href', URIUtil.updateQueryString(window.location.href, 'coverage', 'true'))
          .text('Run Tests WITH Coverage');
      }

        require.config(config);

        /*
        * Global method to detect if we are running headlessly.
        * THIS SHOULD ONLY BE USED TO WORK AROUND PHANTOMJS BUGS
        */
        window.testsRunningHeadless = function () {
            return navigator.userAgent.toLowerCase().indexOf('phantomjs') > 0;
        };

        function getSpecRequire(spec, paths) {
            var specConfig = $.extend(true, {}, config, {
                context: spec,
                paths: paths
            });

            return require.config(specConfig);
        }

        var reporter;
        function setupReporter(jasmineEnv) {
            if (!reporter) {
                reporter = new DelegatingReporter();
                jasmineEnv.addReporter(reporter);

                if (window.testsRunningHeadless()) {
                    reporter.setResultHandler(new ConsoleReporter());
                } else {
                    reporter.setResultHandler(new HtmlReporter(document.getElementById('testResults')));
                }
            }
        }

        function runSpec(spec, callback) {
            var require = getSpecRequire(spec.name, spec.pathConfig);

            require([
                spec.name
            ], function () {
                var jasmineEnv = jasmine.getEnv();
                jasmineEnv.updateInterval = 1000;

                setupReporter(jasmineEnv);
                reporter.afterRunCompleted = callback;
                reporter.specInfo = {
                    specProfile: spec.profile,
                    specFile: spec.name
                };
                jasmineEnv.execute();
                delete globalRequire.s.contexts[spec];
            });
        }

        function requireProfile(profile, callback) {
            require(['test/profiles/' + profile], function (specDefinitions) {
                callback(profile, specDefinitions);
            });
        }

        function getSpecProfiles(callback) {
            var profiles = {};
            var doneCount = 0;
            for (var i = 0; i < specProfiles.length; i++) {
                requireProfile(specProfiles[i], function (profile, specDefinitions) {
                    profiles[profile] = specDefinitions;
                    doneCount++;
                    if (doneCount === specProfiles.length) {
                        callback(profiles);
                    }
                });
            }
        }

        function getSpecList(callback) {
            var specData = [];
            var requestedProfile = getQueryStringValue('specProfile');
            var requestedSpecFile = getQueryStringValue('specFile');

            getSpecProfiles(function (profiles) {
                for (var profile in profiles) {
                    if (!requestedProfile || requestedProfile === profile) {
                        var specGroupDefinition = profiles[profile];
                        for (var spec in specGroupDefinition) {
                            if (!requestedSpecFile || requestedSpecFile === spec) {
                                specData.push({
                                    name: spec,
                                    pathConfig: specGroupDefinition[spec],
                                    profile: profile
                                });
                            }
                        }
                    }
                }

                callback(specData);
            });
        }

        function runTests(onComplete) {
            getSpecList(function (specList) {
                function doNextSpec() {
                    if (specList.length > 0) {
                        var spec = specList.pop();
                        runSpec(spec, doNextSpec);
                    } else {
                        if (onComplete) {
                            onComplete();
                        }
                    }
                }

                doNextSpec();
            });
        }

        function createRunnerUrl(specInfo) {
            var url = '';
            console.log(specInfo);
            url += window.location.protocol + '//';
            url += window.location.host;
            url += window.location.pathname;
            url += '?specProfile=' + specInfo.profile;
            url += '&specFile=' + specInfo.name;
            return url;
        }

        function listTests() {
            getSpecList(function (specList) {
                function writeNextSpec() {
                    if(specList.length > 0) {
                        var spec = specList.pop();
                        var url = createRunnerUrl(spec);
                        var suiteLink = document.createElement('a');
                        suiteLink.setAttribute('href', url);
                        suiteLink.innerHTML = spec.name;

                        var li = document.createElement('li');
                        li.appendChild(suiteLink);
                        document.getElementById('profileList').appendChild(li);
                        writeNextSpec();
                    }
                }

                writeNextSpec();
            });
        }

        $(function() {

            $('#profileToggle').click(function () {
                $('#profileList').toggle();
            });

            listTests();

            runTests(function () {
                console.log('Testing Complete');
                reporter.finalizeResults(function (results) {
                    var passed = true;
                    for (var i = 0; i < results.length; i++) {
                        if (results[i].status === 'FAILED') {
                            passed = false;
                        }
                    }
                    if (window.testsRunningHeadless) {
                        var xmlReporter = new XMLReporter();
                        xmlReporter.createXmlReport(results);
                    }
                    // these two properties let phantomjs know we're done testing.
                    window.testingComplete = true;
                    window.testingPassed = passed;
                });
            });
        });
    });

}());
