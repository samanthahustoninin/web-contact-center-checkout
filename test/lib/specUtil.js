define([
    'jquery',
    'lib/lodash',
    'test/lib/Nonsense'
], function ($, _, Nonsense) {
    'use strict';

    // ===============================================================================
    // Utility Functions
    // ===============================================================================
    var _testNode;
    var _testNodeID;

    var _setTestNodeID = function(value){
        _testNodeID ="testNode_"+ ((value)? value : new Nonsense().uuid());
    };

    var _getTestNodeID = function(){
        return _testNodeID;
    };

    var _getTestNode = function(){
        return _testNode;
    };

    var _prepareTestNode = function () {
        // The bindings specs make frequent use of this utility function to set up
        // a clean new DOM node they can execute code against
        var existingNode = document.getElementById(_getTestNodeID());

        if (existingNode !== null) {
            existingNode.parentNode.removeChild(existingNode);
        }

        _testNode = document.createElement("div");
        _testNode.id = _getTestNodeID();
        document.body.appendChild(_testNode);
    };

    var _clearTestNode = function () {
        var existingNode = document.getElementById(_getTestNodeID());

        if (existingNode){
            existingNode.parentNode.removeChild(existingNode);
            _testNode = null;
        }
    };

    //Spec Util definition
    var SpecUtil = function(){
        var self = this;
        _setTestNodeID();

        self.getTestNode = function(){
            return _getTestNode();
        };

        self.prepareTestNode = function(){
            _prepareTestNode();
            return self.getTestNode();
        };

        self.clearTestNode = function(){
            _clearTestNode();
        };

        self.getNodeId = function(){
            return _getTestNodeID();
        };

        self.randomDate = function (start, end) {
            return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
        };

        self.spyAll = function (object) {
            for (var key in object) {
                if (object.hasOwnProperty(key) && _.isFunction(object[key])) {
                    spyOn(object, key);
                }
            }
        };
    };

    return new SpecUtil();
});
