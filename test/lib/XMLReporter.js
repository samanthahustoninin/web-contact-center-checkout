(function () {
    'use strict';

    function trim(str) {
        return str.replace(/^\s+/, "").replace(/\s+$/, "");
    }

    function escapeInvalidXmlChars(str) {
        var quotRegex = new RegExp('"', 'g');  //my syntax highlighter doesn't like the literal versions. :(
        var aposRegex = new RegExp("'", 'g');
        return str.replace(/\&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/\>/g, "&gt;")
            .replace(quotRegex, "&quot;")
            .replace(aposRegex, "&apos;");
    }

    var XMLReporter =  function () {};

    // need to create a global here just to prevent multiple instances of jasmine reports from running.

    XMLReporter.prototype = {

        createSpecXml: function (spec) {
            var output = '<testcase classname="' + this.getFullName(spec.parent) +
                '" name="' + escapeInvalidXmlChars(spec.description) + '" time="' + spec.duration + '">';

            var failureMsg = "";
            var failures = 0;
            var failureDetails = spec.details;
            for (var i = 0; i < failureDetails.length; i++) {
                var failure = failureDetails[i];
                failures += 1;
                failureMsg += (failures + ": " + escapeInvalidXmlChars(failure.message) + " ");
            }
            if (failureMsg) {
                output += "<failure>" + trim(failureMsg) + "</failure>";
            }
            output += "</testcase>";

            return output;
        },

        createSuiteXml: function (suite) {
            // for JUnit results, let's only include directly failed tests (not nested suites')
            var spec;
            var specs = suite.specs;
            var specOutput = '';
            var failedCount = 0;

            for (var i = 0; i < specs.length; i++) {
                spec = specs[i];
                failedCount += (spec.status == 'FAILED') ? 1 : 0;
                specOutput += "\n  " + this.createSpecXml(spec);
            }
            var output = '\n<testsuite name="' + this.getFullName(suite) +
                '" errors="0" tests="' + specs.length + '" failures="' + failedCount +
                '" time="' + suite.duration + '">';
            output += specOutput;
            output += "\n</testsuite>";

            return output;
        },

        createXmlReport: function (suites) {

            for (var i = 0; i < suites.length; i++) {
                var suite = suites[i];
                var filename = 'TEST-' + this.getFullName(suite, true) + '.xml';
                var output = '<?xml version="1.0" encoding="UTF-8" ?>';

                // only write out top-level suites
                if (suite.parentSuite) {
                    continue;
                }

                output += "\n<testsuites>";
                output += this.createSuiteXml(suite);
                if (suite.suites) {
                    output += this.getNestedOutput(suite.suites);
                }
                output += "\n</testsuites>";
                this.createSuiteResultContainer(filename, output);
            }
        },

        getNestedOutput: function (subSuites) {
            var output = '';
            for (var i = 0; i < subSuites.length; i++) {
                var suite = subSuites[i];
                output += this.createSuiteXml(suite);
                if (suite.suites) {
                    output += this.getNestedOutput(suite.suites);
                }
            }
            return output;
        },

        createSuiteResultContainer: function (filename, xmloutput) {
            jasmine.phantomjsXMLReporterResults = jasmine.phantomjsXMLReporterResults || [];
            jasmine.phantomjsXMLReporterResults.push({
                "xmlfilename" : filename,
                "xmlbody" : xmloutput
            });
        },

        createTestFinishedContainer: function (passed) {
            jasmine.phantomjsXMLReporterPassed = passed;
        },

        getFullName: function (suite, isFilename) {
            var fullName;
            fullName = suite.description;
            for (var parentSuite = suite.parentSuite; parentSuite; parentSuite = parentSuite.parentSuite) {
                fullName = parentSuite.description + '.' + fullName;
            }

            // Either remove or escape invalid XML characters
            if (isFilename) {
                return fullName.replace(/[^\w]/g, "");
            }
            return escapeInvalidXmlChars(fullName);
        }
    };

    // export public
    window.XMLReporter = XMLReporter;
})();
