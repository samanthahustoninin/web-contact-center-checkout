/*global phantom, console, jasmine */

/**
 * PhantomJS script for loading a page and extracting output from
 * the phantomjs report as junit results. Original source:
 * https://github.com/detro/phantomjs-jasminexml-example
 *
 * From the docs:
 * phantomjs_jasminexml_runner.js instead runs the show, launches the tests,
 * extracts the result from the page, then saves it to the directory passed (see source).
 */
(function () {
    'use strict';

    function waitFor(check, onTestPass, onTimeout, _timeoutMs, _freqMs, startTime) {
        var timeoutMs = _timeoutMs || 3000, //< Default Timeout is 3s
            freqMs = _freqMs || 250, //< Default Freq is 250ms
            start = startTime || Date.now();

            setTimeout(function () {
                var condition = check();
                var elapsedMs = Date.now() - start;
                if (condition) {
                    onTestPass();
                } else if (elapsedMs < timeoutMs) {
                    waitFor(check, onTestPass, onTimeout, timeoutMs, freqMs, start);
                } else {
                    onTimeout();
                }
            }, freqMs);
    }

    function checkTestCompletion(page) {
        return page.evaluate(function () {
            return typeof(window.testingComplete) !== "undefined";
        });
    }

    function onComplete(page, onSuccess, onFailure) {
        var fs = require("fs");
        // Retrieve the result of the tests
        var f = null, i, len,
            suitesResults = page.evaluate(function () {
                return jasmine.phantomjsXMLReporterResults;
            });

        // Save the result of the tests in files
        console.log("Writing test results in JUnit format...");

        for (i = 0, len = suitesResults.length; i < len; ++i) {
            try {
                f = fs.open(resultdir + '/' + suitesResults[i].xmlfilename, "w");
                f.write(suitesResults[i].xmlbody);
                f.close();
            } catch (e) {
                console.log(e);
                console.log("phantomjs> Unable to save result of Suite '" + suitesResults[i].xmlfilename + "'");
            }
        }

        // Write out the coverage data if available
        var coverageResults = page.evaluate(function () {
            // Save the code coverage report.
            return window.__coverage__;
        });

        if (coverageResults) {
            console.log("Writing code coverage results...");

            try {
                f = fs.open(resultdir + '/coverage.json', "w");
                f.write(JSON.stringify(coverageResults));
                f.close();
            } catch (e) {
                console.log(e);
                console.log("phantomjs> Unable to save code coverage results: " + coverageResults);
            }
        }

        // Return the correct exit status. '0' only if all the tests passed
        var testingPassed = page.evaluate(function () {
            return window.testingPassed;
        });

        page.close();
        if (!testingPassed) {
            onFailure();
        } else {
            onSuccess();
        }
    }

    function onTimeout() {
        console.error("Tests stopped running due to timeout");
        phantom.exit(1);
    }

    function testPage(htmlrunner, resultdir, onSuccess, onFailure) {
        var page = require("webpage").create();

        // Echo the output of the tests to the Standard Output
        page.onConsoleMessage = function (msg) {
            console.log(msg);
        };

        page.open(htmlrunner, function (status) {
            if (status === "success") {
                waitFor(
                    function () {
                        return checkTestCompletion(page);
                    },
                    function () {
                        return onComplete(page, onSuccess, onFailure);
                    },
                    onTimeout,
                    600000 // set the timeout to 10 minutes
                );
            } else {
                console.log("phantomjs> Could not load '" + htmlrunner + "'.");
                phantom.exit(1);
            }
        });
    }

    function onPageFailure() {
        console.log("FAILURES!");
        phantom.exit(1);
    }

    if (phantom.args.length < 2) {
        console.log("Usage: phantom_test_runner.js HTML_RUNNER RESULT_DIR");
        phantom.exit(1);
    }

    var resultdir = phantom.args[0];
    var toRun = phantom.args.slice(1);
    function runNext () {
        if (toRun.length > 0) {
            var runner = toRun.pop();
            testPage(runner, resultdir, runNext, onPageFailure);
        } else {
            phantom.exit(0);
        }
    }

    runNext();

})();
