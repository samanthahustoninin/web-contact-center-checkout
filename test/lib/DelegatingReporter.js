/*global jasmine*/
/**
 * Jasmine reporter that passes results off to a callback.
 */
(function (global) {
    'use strict';

    var SpecStatus = {
        SKIPPED: 'SKIPPED',
        FAILED: 'FAILED',
        PASSED: 'PASSED'
    };

    function elapsed(startTime, endTime) {
        return (endTime - startTime) / 1000;
    }

    function noop() {}

    function getSpecStatus(spec) {
        var results = spec.results();
        if (results.skipped) {
            return SpecStatus.SKIPPED;
        }
        if (results.passed()) {
            return SpecStatus.PASSED;
        } else {
            return SpecStatus.FAILED;
        }
    }

    function getFailureDetails(spec) {
        var detailEntry, result;
        var details = [];
        var resultItems = spec.results().getItems();
        for (var i = 0; i < resultItems.length; i++) {
            result = resultItems[i];
            if (!result.passed()) {
                detailEntry = {
                    message: result.message
                };
                if (result.trace.stack) {
                    detailEntry.stack = result.trace.stack;
                }

                details.push(detailEntry);
            }
        }

        return details;
    }

    function initSuiteResultsIfNeeded(suite) {
        if (!suite.simpleResults) {
            suite.simpleResults = {
                description: suite.description,
                specs: [],
                suites: [],
                status: 'SKIPPED'
            };
        }

        if (suite.parentSuite) {
            initSuiteResultsIfNeeded(suite.parentSuite);
        }
    }

    function runInEmptyStack(fn, arg) {
        if (jasmine.Clock.isInstalled()) {
            jasmine.Clock.uninstallMock();
        }
        window.setTimeout(function () {
            fn(arg);
        }, 1);
    }


    function DelegatingReporter() {
        this._testResults = [];
    }

    DelegatingReporter.prototype = {
        setResultHandler: function (handler) {
            this.resultHandler = handler;
        },

        reportSpecStarting: function (spec) {
            spec.suite.startTime = new Date();
            spec.startTime = new Date();
        },

        reportSpecResults: function (spec) {
            var suite = spec.suite;
            initSuiteResultsIfNeeded(suite);

            var simpleResults = {
                parent: spec.suite.simpleResults,
                description: spec.description,
                duration: elapsed(spec.startTime, new Date()),
                status: getSpecStatus(spec),
                details: []
            };

            if (simpleResults.status === SpecStatus.FAILED) {
                suite.simpleResults.status = SpecStatus.FAILED;
                simpleResults.details = getFailureDetails(spec);
            } else if (simpleResults.status === SpecStatus.PASSED && suite.simpleResults.status == SpecStatus.SKIPPED) {
                suite.simpleResults.status = SpecStatus.PASSED;
            }

            suite.simpleResults.specs.push(simpleResults);
            spec.simpleResults = simpleResults;

            if (this.resultHandler && this.resultHandler.onSpecResults) {
                this.resultHandler.onSpecResults(spec.simpleResults);
            }
        },

        reportSuiteResults: function (suite) {
            initSuiteResultsIfNeeded(suite);
            var parent = suite.parentSuite;
            if (parent) {
                initSuiteResultsIfNeeded(parent);
                parent.simpleResults.suites.push(suite.simpleResults);
                suite.simpleResults.parent = parent.simpleResults;
            } else {
                suite.simpleResults.specInfo = this.specInfo;
            }

            suite.simpleResults.duration = suite.startTime ? elapsed(suite.startTime, new Date()) : 0;

            if (this.resultHandler && this.resultHandler.onSuiteResults) {
                this._testResults.push(suite.simpleResults);
                this.resultHandler.onSuiteResults(suite.simpleResults);
            }
        },

        reportRunnerResults: function (runner) {
            var results = runner.suites();
            if (this.afterRunCompleted) {
                runInEmptyStack(this.afterRunCompleted, results);
            }
        },

        finalizeResults: function (callback) {
            if (this.resultHandler && this.resultHandler.finalizeResults) {
                this.resultHandler.finalizeResults(this._testResults);
                if (callback) {
                    callback(this._testResults);
                }
            }
        },

        getNestedOutput: noop,
        createSuiteResultContainer: noop,
        createTestFinishedContainer: noop,
        getFullName: noop,

        log: function (str) {
            var console = jasmine.getGlobal().console;
            if (console && console.log) {
                console.log(str);
            }
        }
    };

    // export public
    global.DelegatingReporter = DelegatingReporter;
})(this);
