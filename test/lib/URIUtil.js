define([], function () {
    'use strict';

    return {
        /*
         * Update a query parameter in the page's current location URI. If the provided
         * key does not exist, this method will add it to the URI. If it does exist, the
         * value will be updated.
         *
         * @return A new URI containing the updated query parameter key-value pair.
         */
        updateQueryString: function (url, key, value) {
            if (!url) {
                url = window.location.href;
            }

            var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi");
            var hash = url.split('#');

            if (re.test(url)) {
                if (typeof value !== 'undefined' && value !== null) {
                    return url.replace(re, '$1' + key + "=" + value + '$2$3');
                } else {
                    url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
                    if (typeof hash[1] !== 'undefined' && hash[1] !== null) {
                        url += '#' + hash[1];
                    }

                    return url;
                }
            } else {
                if (typeof value !== 'undefined' && value !== null) {
                    var separator = url.indexOf('?') !== -1 ? '&' : '?';
                    url = hash[0] + separator + key + '=' + value;
                    if (typeof hash[1] !== 'undefined' && hash[1] !== null) {
                        url += '#' + hash[1];
                    }
                    return url;
                }

                return url;
            }
        }
    };
});