(function (global) {
    'use strict';
    var console = global.console;

    function getAncestorDescriptions(spec) {
        var ancestors = [];
        var item = spec;
        while (item.parent &&
               item.parent.description !== null &&
               item.parent.description !== undefined
        ) {
            ancestors.push(item.parent.description);
            item = item.parent;
        }

        return ancestors;
    }

    function aggregateSpecResult(aggregateResults, spec) {
        var i;
        if (spec.status == 'SKIPPED') {
            aggregateResults.skipped += 1;
        } else if (spec.status == 'PASSED') {
            aggregateResults.passed += 1;
        } else if (spec.status == 'FAILED') {
            aggregateResults.failed += 1;

            var ancestorDescriptions = getAncestorDescriptions(spec);
            var scope = aggregateResults.failureDetails;
            for (i = 0; i < ancestorDescriptions.length; i++) {
                var description = ancestorDescriptions[i];
                if (!scope[description]) {
                    scope[description] = {};
                }
                scope = scope[description];
            }

            scope[spec.description] = spec.details;
        }
    }

    function aggregateSuiteResults(aggregateResults, suiteResult) {
        var specResult;
        var specs = suiteResult.specs;
        for (var i = 0; i < specs.length; i++) {
            specResult = specs[i];
            aggregateSpecResult(aggregateResults, specResult);
        }
    }

    function indent(reporter) {
        reporter._indentStr += '  ';
    }

    function unindent(reporter) {
        reporter._indentStr = reporter._indentStr.slice(0,-2);
    }

    function log(reporter, message) {
        console.log(reporter._indentStr + message);
    }

    function logDetailEntry(reporter, detailItem) {
        var i, entry, key;
        if (detailItem instanceof Array) {
            for (i = 0; i < detailItem.length; i++) {
                entry = detailItem[i];
                log(reporter, entry.message);
                if (entry.stack) {
                    log(reporter, entry.stack);
                }
            }
        } else {
            for (key in detailItem) {
                log(reporter, key);
                indent(reporter);
                logDetailEntry(reporter, detailItem[key]);
                unindent(reporter);
            }
        }
    }

    var ConsoleReporter = function () {
        this._results = {
            skipped: 0,
            passed: 0,
            failed: 0,
            failureDetails: {}
        };

        this._indentStr = '';
    };

    ConsoleReporter.prototype = {
        onSuiteResults: function (suiteResult) {
            aggregateSuiteResults(this._results, suiteResult);
        },

        logFailureDetails: function () {
            if (this._results.failed > 0) {
                console.log('*** FAILURE DETAILS ***');
                logDetailEntry(this, this._results.failureDetails);
                console.log('\n');
            }
        },

        logTestSummary: function () {
            var skipped = this._results.skipped;
            var passed = this._results.passed;
            var failed = this._results.failed;
            var count = skipped + passed + failed;

            var summaryMessage = (failed > 0) ? "TESTING FAILED" : "SUCCESS!";

            log(this, summaryMessage);
            indent(this);
            log(this, count + " tests run.");
            indent(this);
            log(this, failed + " FAILURES");
            log(this, passed + " PASSED");
            log(this, skipped + " SKIPPED");
            console.log('\n\n');
        },

        finalizeResults: function () {
            global.testResults = this._results;
            console.log('\n\n');
            this.logFailureDetails();
            this.logTestSummary();
        }
    };

    global.ConsoleReporter = ConsoleReporter;
}(this));
