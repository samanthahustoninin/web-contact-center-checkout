(function (global) {
    'use strict';

    function HtmlReporter(targetElement) {
        targetElement.innerHTML = '';
        this._checklist = document.createElement('ul');
        this._checklist.classList.add('checklist');
        targetElement.appendChild(this._checklist);

        this._suiteResults = document.createElement('div');
        this._suiteResults.classList.add('suite-results');
        targetElement.appendChild(this._suiteResults);
        var toggleDisplayLink = document.createElement('a');
        toggleDisplayLink.classList.add('display-toggle');
        toggleDisplayLink.setAttribute('href', '#');
        toggleDisplayLink.innerHTML = 'Show only failures';
        toggleDisplayLink.addEventListener('click', function () {
            targetElement.classList.toggle('failures-only');
            if (targetElement.classList.contains('failures-only')) {
                toggleDisplayLink.innerHTML = 'Show all results';
            } else {
                toggleDisplayLink.innerHTML = 'Show only failures';
            }
        });
        this._suiteResults.appendChild(toggleDisplayLink);
    }

    var STATUS_INDICATORS = {
        PASSED: '&#10003;',
        FAILED: '&#10007;',
        SKIPPED: '&#8226;'
    };

    function createRunnerUrl(specInfo) {
        var url = '';
        url += window.location.protocol + '//';
        url += window.location.host;
        url += window.location.pathname;
        url += '?specProfile=' + specInfo.specProfile;
        url += '&specFile=' + specInfo.specFile;
        return url;
    }

    function buildFailureDetails(failureDetails) {
        var detailsElement = document.createElement('div');
        detailsElement.classList.add('indented');
        detailsElement.classList.add('details');

        for (var i = 0; i < failureDetails.length; i++) {
            var detail = failureDetails[i];
            var detailElement = document.createElement('div');
            detailElement.classList.add('detail');

            var messageElement = document.createElement('div');
            messageElement.appendChild(document.createTextNode(detail.message));
            detailElement.appendChild(messageElement);

            if (detail.stack) {
                var stackElement = document.createElement('pre');
                stackElement.appendChild(document.createTextNode(detail.stack));
                detailElement.appendChild(stackElement);
            }

            detailsElement.appendChild(detailElement);
        }

        return detailsElement;
    }

    function buildSpecElement(spec) {
        var specElement = document.createElement('div');
        specElement.classList.add('indented');
        specElement.classList.add(spec.status.toLowerCase());
        if (spec.status != 'FAILED') {
            specElement.classList.add('no-failures');
        }
        specElement.innerHTML = spec.description;
        if (spec.details) {
            specElement.appendChild(buildFailureDetails(spec.details));
        }
        return specElement;
    }

    function buildSuiteElement(suite) {
        var suiteElement = document.createElement('div');
        if (!suite.parent) {
            suiteElement.classList.add('suite-summary');
        } else {
            suiteElement.classList.add('indented');
        }

        if (suite.status != 'FAILED') {
            suiteElement.classList.add('no-failures');
        }

        if (suite.specInfo) {
            var url = createRunnerUrl(suite.specInfo);
            var suiteLink = document.createElement('a');
            suiteLink.setAttribute('href', url);
            suiteLink.innerHTML = suite.description;
            suiteElement.appendChild(suiteLink);
        } else {
            suiteElement.appendChild(document.createTextNode(suite.description));
        }

        for (var i = 0; i < suite.specs.length; i++) {
            var spec = suite.specs[i];
            suiteElement.appendChild(buildSpecElement(spec));
        }
        for (i = 0; i < suite.suites.length; i++) {
            var subSuite = suite.suites[i];
            suiteElement.appendChild(buildSuiteElement(subSuite));
        }

        return suiteElement;
    }

    HtmlReporter.prototype = {
        appendChecklistItem: function (status) {
            var statusClass = status.toLowerCase();
            var li = document.createElement('li');
            li.classList.add(statusClass);
            li.innerHTML = STATUS_INDICATORS[status];
            this._checklist.appendChild(li);
            this._checklist.appendChild(document.createTextNode('\u200B'));
        },

        onSpecResults: function (results) {
            this.appendChecklistItem(results.status);
        },

        onSuiteResults: function (suite) {
            if (!suite.parent) {
                var resultElement = buildSuiteElement(suite);
                this._suiteResults.appendChild(resultElement);
            }
        }
    };

    global.HtmlReporter = HtmlReporter;
}(this));
