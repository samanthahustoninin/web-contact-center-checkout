define([
    'core!lib/bluebird',
    'core!util/errorCallbackFactory',

    'checkout/services/checkoutService',
    'checkout/views/TextView'
], function (Promise, errorCallbackFactory, checkoutService, TextView) {
    'use strict';

    describe('The text view', function () {
        var resolvePromise, rejectPromise;

        beforeEach(function () {
            spyOn(checkoutService, 'getData');
            checkoutService.getData
                .andReturn(new Promise(function (resolve, reject) {
                    resolvePromise = resolve;
                    rejectPromise = reject;
                }));
        });

        afterEach(function () {
            rejectPromise(); //just in case it got skipped in the test.
        });

        it('starts in a loading state', function () {
            var textView = new TextView();
            expect(textView.isLoading()).toBe(true);
        });

        it('will display a failure if the service returns an error', function () {
            var errorObject = {};
            var errorSpy = jasmine.createSpy('errorCallback');
            spyOn(errorCallbackFactory, 'createCallback').andReturn(errorSpy);

            var textView = new TextView();

            runs(function () {
                expect(errorCallbackFactory.createCallback).toHaveBeenCalledWith({namespace: 'checkout.textView'});
                rejectPromise(errorObject);
            });

            waitsFor(function () {
                return textView.isLoading() === false;
            }, 'async to finish', 500);

            runs(function () {
                expect(errorSpy).toHaveBeenCalledWith(errorObject);
            });
        });

        it('references model data returned by the service', function () {
            var model = {};
            var textView = new TextView();

            runs(function () {
                resolvePromise(model);
            });

            waitsFor(function () {
                return textView.isLoading() === false;
            }, 'async to finish', 500);

            runs(function () {
                expect(textView.model()).toBe(model);
            });
        });

    });

});
