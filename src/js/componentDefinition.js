define([
    'checkout/siteMap',
    'checkout/navEntries',
    'checkout/routes',
    'checkout/rootView'
], function (siteMap, navEntries, routes, rootView) {
    'use strict';

    return {
        componentMap: siteMap,
        componentVersion: 2,
        navEntries: navEntries,
        rootView: rootView,
        routeContext: rootView,
        routeInfo: routes,
        
        //Values replaced during build
        stringsPath: '%STRINGS_PATH%',
        cssUrl: '%CSS_URL%',
        version: '%VERSION%'
    };
});
