define([
    //views
    'core!viewModels/SwitchedView',

    'checkout/viewModels/keywords/KeywordSetList',
    'checkout/viewModels/keywords/KeywordSetView',
    'checkout/viewModels/keywords/KeywordView',
    'checkout/viewModels/EvaluatorsView',
    'checkout/viewModels/EvaluatorView',
    'checkout/viewModels/ProductList',
    'checkout/viewModels/ProductView',
    'checkout/viewModels/OrderList',
    'checkout/viewModels/OrderView',
    //binding handlers
    'checkout/bindingHandlers/integerPicker',
    //Ko extensions
    'checkout/extensions/revertible'
], function (
    SwitchedView,

    KeywordSetList,
    KeywordSetView,
    KeywordView,
    EvaluatorsView,
    EvaluatorView,
    ProductList,
    ProductView,
    OrderList, 
    OrderView
    ) {
    'use strict';

    var rootView = {
        subViews: new SwitchedView({
            keywordSetList: KeywordSetList,
            keywordSetView: KeywordSetView,
            keywordView: KeywordView,
            evaluatorList: EvaluatorsView,
            evaluatorView: EvaluatorView,
            productList: ProductList,
            productView: ProductView,
            orderList: OrderList,
            orderView: OrderView
        }),

        dispose: function () {
            rootView.subViews.setActiveView(); // clear active views
        }
    };

    return rootView;
});