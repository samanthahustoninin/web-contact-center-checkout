define(['core!util/NavBuilder',
        'core!lib/lodash',
        'core!lib/bluebird',
        'core!services/authService',
        'core!services/featureToggleService',
        'checkout/routePermissions'], function (NavBuilder, _, Promise, authService, featureToggleService, routePermissions) {
    'use strict';

    var navBuilder = new NavBuilder();

    var qualityNav = navBuilder
        .entry('checkout.navMenu.checkout', '#checkout')
        .requireAuth.apply(this, routePermissions.QUALITY);

        qualityNav.subLink('checkout.products.title', '#checkout/admin/products');

        qualityNav.subLink('checkout.orders.title', '#checkout/admin/orders');

    return navBuilder.navTree;
});

