define([
    'core!jquery',
    'core!lib/bluebird',
    'core!lib/lodash',
    'core!lib/knockout',
    'core!services/apiCore',
    'checkout/models/Product'
], function ($, Promise, _, ko, apiCore, Products) {
    'use strict';


    var productsService = {

        getProducts: function() {
            return Promise.cast($.getJSON('https://localhost:8080/products')).then(function (results) {
                var models = _.map(results, function (productData) {
                    return new Products(productData);
                });
                return {
                    pageCount: 1,
                    pageNumber: 1,
                    pageSize: 100,
                    entities: models
                };
            });
        },

        getProduct: function(id) {
            return Promise.cast($.getJSON('https://localhost:8080/products/' + id)).then(function (result) {
                return new Products(result);
            });
        },

        createProduct: function(productModel) {
            var productData = ko.toJS(productModel);
            return Promise.cast($.ajax("https://localhost:8080/products", {
                data: productData,
                type: "post"
            }));
        },

        updateProduct: function(productModel) {
            var productData = ko.toJS(productModel);
            return Promise.cast($.ajax("https://localhost:8080/products/" + productData.id, {
                data: productData,
                type: "put"
            }));
        },

        deleteProducts: function(id) {
            return Promise.cast($.ajax("https://localhost:8080/products/" + id, {
                type: "delete"
            }));
        }

    };

    return productsService;
}); //end define()