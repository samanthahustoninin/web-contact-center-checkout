define([
    'core!jquery',
    'core!lib/bluebird',
    'core!lib/lodash',
    'core!lib/knockout',
    'core!services/apiCore',
    'checkout/models/Order'
], function ($, Promise, _, ko, apiCore, Order) {
    'use strict';


    var orderService = {

        getOrders: function() {
            return Promise.cast($.getJSON('https://localhost:8080/orders')).then(function (results) {
                var models = _.map(results, function (orderData) {
                    return new Order(orderData);
                });
                return {
                    pageCount: 1,
                    pageNumber: 1,
                    pageSize: 100,
                    entities: models
                };
            });
        },
        getOrder: function(id) {
            return Promise.cast($.getJSON('https://localhost:8080/orders/' + id)).then(function (result) {
                return new Order(result);
            });
        },
        updateOrder: function(orderModel) {
            var orderData = ko.toJS(orderModel);
            return Promise.cast($.ajax("https://localhost:8080/orders/" + orderData.id, {
                data: orderData,
                type: "put"
            }));
        }
    };

    return orderService;
}); //end define()