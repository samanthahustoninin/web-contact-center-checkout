define([
    'core!jquery',
    'core!lib/bluebird',
    'core!lib/lodash',
    'core!lib/knockout',
    'core!services/apiCore',
    'checkout/models/KeywordSet'
], function ($, Promise, _, ko, apiCore, KeywordSet) {
    'use strict';


    var keywordService = {

        getKeywordSets: function(options) {
            return apiCore.get('/checkout/keywordsets', options);
        },

        getKeywordSet: function(keywordSetId) {
            return apiCore.get('/checkout/keywordsets/' + keywordSetId)
            .then(function (response) {
                return new KeywordSet(response);
            });
        },

        createKeywordSet: function(keywordSetModel) {
            var keywordSetData = ko.toJS(keywordSetModel);
            return apiCore.post('/checkout/keywordsets', keywordSetData).then(function (response) {
                return new KeywordSet(response);
            });
        },

        updateKeywordSet: function(keywordSetModel) {
            var keywordSetPath = keywordSetModel.selfUri().replace(/(^.+api\/v\d)(.+)/, '$2');
            var keywordSetData = ko.toJS(keywordSetModel);
            return apiCore.put(keywordSetPath, keywordSetData).then(function (response) {
                return new KeywordSet(response);
            });
        },

        batchDeleteKeywordSets: function(options) {
            return apiCore.del('/checkout/keywordsets', options);
        }

    };

    return keywordService;
}); //end define()