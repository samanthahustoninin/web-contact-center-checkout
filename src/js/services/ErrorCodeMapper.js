define([], function() {
    'use strict';

    return {
        QUALITY_CALIBRATION_CREATE_PERMISSION_CHECK_FAILED: "quality.serviceErrors.code.qualityCalibrationCreatePermissionCheckFailed",
        QUALITY_EVALUATION_FORM_DOESNT_EXIST: "quality.serviceErrors.code.qualityEvaluationFormDoesntExist",
        QUALITY_CALIBRATION_EXPERT_EVALUATOR_NOT_QUALITY_EVALUATOR: "quality.serviceErrors.code.qualityCalibrationExpertEvaluatorNotEvaluator",
        QUALITY_CALIBRATION_EXPERT_EVALUATOR_DOESNT_EXIST: "quality.serviceErrors.code.qualityCalibrationExpertEvaluatorDoesNotExist",
        QUALITY_EVALUATION_NO_CONVERSATION: "quality.serviceErrors.code.qualityEvaluationNoConversation",
        QUALITY_EVALUATION_NO_EVALUATION_FORM_OR_CONTEXT: "quality.serviceErrors.code.qualityEvaluationNoEvaluationFormOrContext",
        QUALITY_EVALUATION_NO_EVALUATOR: "quality.serviceErrors.code.qualityEvaluationNoEvaluator",
        QUALITY_EVALUATION_NO_AGENT: "quality.serviceErrors.code.qualityEvaluationNoAgent",
        QUALITY_CONVERSATION_DOESNT_EXIST: "quality.serviceErrors.code.qualityEvaluationConversationDoesntExist",
        QUALITY_EVALUATION_FORM_NOT_PUBLISHED: "quality.serviceErrors.code.qualityEvaluationFormNotPublished",
        QUALITY_EVALUATION_EVALUATOR_DOESNT_EXIST: "quality.serviceErrors.code.qualityEvaluationEvaluatorDoesntExist",
        QUALITY_EVALUATION_AGENT_DOESNT_EXIST: "quality.serviceErrors.code.qualityEvaluationAgentDoesntExist",
        QUALITY_EVALUATION_CREATE_PERMISSION_CHECK_FAILED: "quality.serviceErrors.code.qualityEvaluationPermissionCheckFailed",
        UNEXPECTED_ASSIGN_EVALUATION_ERROR: "quality.serviceErrors.code.qualityEvaluationUnexpectedError",
        UNEXPECTED_METERED_ASSIGNMENT_ERROR: "quality.serviceErrors.code.qualityMeteredEvaluationAssignmentError",
        UNEXPECTED_CALIBRATION_ERROR: "quality.serviceErrors.code.qualityCalibrationUnexpectedError"
    };
});