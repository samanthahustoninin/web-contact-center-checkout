define(function() {
    'use strict';

    // Enum for sets of permissions/roles that are required to navigate to a particular route.
    var routePermissions = {
        EVALUATION_FORMS: ['role:qualityAdmin', 'perm:quality:evaluationForm:add', 'perm:quality:evaluationForm:edit', 'perm:quality:evaluationForm:delete'],
        EVALUATORS: ['role:qualityAdmin', 'perm:quality:evaluation:add'],
        RECORDING_MANAGEMENT: [
            'perm:recording:orphanRecording:view',
            'perm:recording:settings:editScreenRecordings',
            'perm:recording:screenRecording:view',
            'perm:recording:screenRecording:stop'],
        RETENTION_POLICIES: ['role:qualityAdmin', 'perm:recording:retentionPolicy:view'],
        RETENTION_POLICY_NEW: ['role:qualityAdmin', 'perm:recording:retentionPolicy:add'],
        RETENTION_POLICY_UPDATE: ['role:qualityAdmin', 'perm:recording:retentionPolicy:edit', 'perm:recording:retentionPolicy:view'],
        ENCRYPTION_KEYS: ['role:qualityAdmin', 'perm:recording:encryptionKey:view'],
        KEYWORD_SETS: ['perm:quality:keywordset:view'],
        KEYWORD_SET_NEW: ['perm:quality:keywordset:add'],
        KEYWORD_SET_UPDATE: ['perm:quality:keywordset:edit', 'perm:quality:keywordset:view'],
        ORPHAN_RECORDINGS: ['perm:recording:orphanRecording:view']
    };

    // Concat of all the main pages
    routePermissions.QUALITY = [].concat(routePermissions.EVALUATION_FORMS)
        .concat(routePermissions.RETENTION_POLICIES)
        .concat(routePermissions.EVALUATION_FORMS)
        .concat(routePermissions.ENCRYPTION_KEYS)
        .concat(routePermissions.RECORDING_MANAGEMENT);
    return routePermissions;
});