define([
    'core!lib/lodash',
    'core!lib/bluebird',
    'core!lib/knockout',
    'core!lib/base64',
    'checkout/models/Product',
    'core!viewModels/ViewModel',
    'checkout/services/productService',
    'core!services/loggingService',
    'core!services/routeService',
    'core!viewModels/widgets/dataGridCells/enumFilter',
    'core!services/qm/qualityAuth',
    'core!services/featureToggleService',
    'core!util/errorCallbackFactory'
], function (_, Promise, ko, base64, Product, ViewModel, productService, LOG, routeService, EnumFilter, qualityAuth, featureToggleService, errorCallbackFactory) {


    'use strict';

    /**
     * The KeywordsList class.
     * @class
     */
    var ProductList = ViewModel.extend(function () {
        var self = this;

        var PRODUCT_COLUMNS = ko.observableArray([
            {
                id: 'id',
                label: 'checkout.products.productId'
            },
            {
                id: 'name',
                label: 'checkout.products.productName'
            },
            {
                id: 'description',
                label: 'checkout.products.product.description'
            },
            {
                id: 'price',
                label: 'checkout.products.product.price.label'
            },
            {
                id: 'tags',
                label: 'checkout.products.product.tag.label'
            }
        ]);


        self.gridOptions = ko.observable({
            pageCount: 1,
            pageNumber: 1,
            pageSize: 25
        });

        self.product = ko.observable(); // This is actually the data that is retrieved from the service
        self.productGrid = ko.observable();  // not sure what this is
        self.newProductHref = '#checkout/admin/products/new'; // create a new product page
        self.loading = ko.observable(true);

        /**
        * For future code ppl,
        * we stole this code from the quality component
        * We didn't want to deal with permission stuff,
        * So keywords = products
        * AND evaluators = orders
        * Just create an admin user with all quality perms and you should be fine
        */
        self.canAdd = qualityAuth.canAddKeywordSet ? qualityAuth.canAddKeywordSet() : false;
        self.canDelete = qualityAuth.canDeleteKeywordSet ? qualityAuth.canDeleteKeywordSet() : false;
        self.canEdit = qualityAuth.canEditKeywordSet ? qualityAuth.canEditKeywordSet() : false;

        self.refreshGrid = function() {
            // var options = {};

            // if (incomingOptions){
            //     if (incomingOptions.pageSize > self.gridOptions().pageSize){
            //         options.pageNumber = 1;
            //     } else {
            //         options.pageNumber = incomingOptions.pageNumber;
            //     }
            //     options.pageSize = incomingOptions.pageSize;
            // }

            // if (self.product()){
            //     options.name = self.product();
            // }

            productService.getProducts()
                .then(function (response) {
                    self.gridOptions({
                        pageCount: response.pageCount,
                        pageNumber: response.pageNumber,
                        pageSize: response.pageSize
                    });
                    var rows = _.map(response.entities, function(productData){
                        var productNameCell = {
                            label: productData.name,
                            href: '#checkout/admin/products/update/' + productData.id()
                        };

                        var price = !!productData.price ? productData.price : "";

                        var tag = productData.tag;
                        
                        return {
                            'id': productData.id,
                            'name': productNameCell,
                            'description': productData.desc,
                            'price': price,
                            'tags': tag,
                            'selfUri': productData.selfUri
                        };
                    });
                    self.productGrid().rows(rows);
                }).catch(errorCallbackFactory.createCallback({
                    "namespace": "quality.products.productRequest"
                })).finally(function(){
                    self.loading(false);
                });
        };

        self.productGrid({
                columns: PRODUCT_COLUMNS,
                rows: ko.observableArray([]),
                selectedRows: ko.observableArray([]),
                dataOptions: self.gridOptions,
                onDataOptionsChange: self.refreshGrid,
                loading: self.loading
            });
        self.refreshGrid();

        function performRefresh(){
            self.refreshGrid();
            self.productGrid().selectedRows([]);
        }

        self.deleteSelectedRows = function () {
            self.loading(true);
            var selectedRows = self.productGrid().selectedRows();
            var id = selectedRows[0].id();

            productService.deleteProducts(id)
            .then(function(){ performRefresh(); })
            .catch(errorCallbackFactory.createCallback({
                "namespace": 'quality.products.deleteError'
            }));
        };

        self.areRowsSelected = self.computed(function(){
            return self.productGrid() && self.productGrid().selectedRows().length > 0;
        });

        self.clearFields = function(){
            self.product("");
            self.refreshGrid();
        };

    });

    return ProductList;
});