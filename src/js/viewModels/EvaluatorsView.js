define([
    'core!lib/lodash',
    'core!lib/knockout',
    'core!moment',
    'core!services/qm/qualityAuth',
    'core!viewModels/dashboards/qualityAdminDashboard/EvaluatorActivityWidget',
    'core!viewModels/ViewModel',
    'core!services/qm/qualityAdminState',
    'core!viewModels/privileged/qm/policies/editors/DateRangeEditorView'
], function (
    _,
    ko,
    moment,
    qualityAuthorization,
    EvaluatorActivityWidget,
    ViewModel,
    qualityAdminState,
    DateRangeEditorView
) {
    'use strict';


    return ViewModel.extend(function () {
        var self = this;

        var startOfMonth = moment().startOf('month').toDate();
        var endOfMonth = moment().endOf('month').toDate();
        self.activityDateRange = ko.observableArray([startOfMonth.toISOString() + '/' + endOfMonth.toISOString()]);
        qualityAdminState.timeFrame(self.activityDateRange());
        self.dateRangesView = self.subView(DateRangeEditorView, self.activityDateRange);

        self.evaluatorActivityWidget = self.subView(EvaluatorActivityWidget, self.activityDateRange);
        self.canViewEvaluationDashlets = qualityAuthorization.canAddEvaluation();

        self.subscribe(self.activityDateRange, function(newDateRange){
            qualityAdminState.timeFrame(newDateRange);
        });
    });

});