define([
    'core!lib/lodash',
    'core!lib/knockout',
    'core!lib/bluebird',
    'core!moment',
    'core!viewModels/ViewModel',
    'core!viewModels/privileged/qm/policies/editors/DateRangeEditorView',
    'core!viewModels/dashboards/qualityAdminDashboard/AgentActivityWidget',
    'core!util/errorCallbackFactory',
    'core!services/authService',
    'core!services/userService',
    'core!services/qm/qualityAuth',
    'core!services/qualityManagementService',
    'core!services/qm/qualityAdminState',
    'core!services/loggingService'
], function (
    _,
    ko,
    Promise,
    moment,
    ViewModel,
    DateRangeEditorView,
    AgentActivityWidget,
    errorCallbackFactory,
    authService,
    userService,
    qualityAuth,
    qmService,
    qualityAdminState,
    loggingService
) {
    'use strict';

    var PENDING_EVALUATION_OPTIONS = {
        evaluationState: [qmService.PENDING, qmService.INPROGRESS]
    };

    var COMPLETED_EVALUATION_OPTIONS = {
        evaluationState: [qmService.FINISHED]
    };

    var PENDING_GRID_COLUMNS = [
        {
            id: 'assignedDate',
            label: 'evaluatorDashboard.pendingEvaluations.assignedDateTime'
        },
        {
            id: 'agent',
            label: 'evaluatorDashboard.pendingEvaluations.agent'
        },
        {
            id: 'form',
            label: 'evaluatorDashboard.pendingEvaluations.formName'
        },
        {
            id: 'status',
            label: 'evaluatorDashboard.pendingEvaluations.status'
        },
        {
            id: 'calibration',
            label: "evaluatorDashboard.pendingEvaluations.isForCalibration"
        }
    ];

    var COMPLETED_GRID_COLUMNS = [
        {
            id: 'assignedDate',
            label: 'evaluatorDashboard.completedEvaluations.assignedDateTime'
        },
        {
            id: 'releaseDate',
            label: 'evaluatorDashboard.completedEvaluations.releaseDateTime'
        },
        {
            id: 'agent',
            label: 'evaluatorDashboard.completedEvaluations.agent'
        },
        {
            id: 'form',
            label: 'evaluatorDashboard.completedEvaluations.formName'
        },
        {
            id: 'score',
            label: "evaluatorDashboard.completedEvaluations.score"
        },
        {
            id: 'calibration',
            label: "evaluatorDashboard.completedEvaluations.isForCalibration"
        }
    ];

    //this function is only here because Math.round() by itself sometimes does not produce correct results
    //i.e if you put in Math.round(1.005 * 100) / 100, you would 1.00, instead of 1.01
    var roundUpDecimals = function(num){
        return +(Math.round(num + "e+2") + "e-2");
    };

    function makeEvaluationRow(evaluation) {
        try {
            var rowData = {
                id: evaluation.id,

                agent: evaluation.agent,
                assignedDate: moment(evaluation.assignedDate).toDate(),
                calibration: evaluation.calibration,
                conversation: evaluation.conversation,
                evaluationForm: evaluation.evaluationForm,
                releaseDate: evaluation.releaseDate ? moment(evaluation.releaseDate).toDate() : null,
                score: ko.observable(),
                status: evaluation.status.toLowerCase()
            };

            //Add the score post-row creation
            //TODO: remove this when the query endpoint contains answer details
            if (isCompleted(evaluation)) {
                qmService.getAssignedEvaluation(evaluation.conversation.id, evaluation.id)
                    .then(function (result) {
                        rowData.score(roundUpDecimals(result.answers.totalScore));
                    }).catch(function () {
                        rowData.score(ko.i18n.renderString('evaluatorDashboard.score.unavailable'));
                    });
            }

            return rowData;
        } catch (e) {
            var evaluationId = evaluation ? evaluation.id : "undefined";
            loggingService.error("Could not generate a row for evaluation " + evaluationId + ". " + e);
            return;
        }
    }

    function isCompleted(evaluation) {
        return (evaluation.status.toLowerCase() == qmService.FINISHED.toLowerCase());
    }

    function populateGrid(gridModel, evaluations) {
        gridModel.rows(_.chain(evaluations)
            .map(makeEvaluationRow)
            .compact()
            .valueOf());
    }

    function getAssignedEvaluations(options) {
        options.expand = ['agent'];
        return qmService.getAssignedEvaluations(options);
    }


    /**
     *  Evaluator Dashboard
     */
    var EvaluatorDashboard = ViewModel.extend(function () {

        //
        // Private Variables
        //

        var self = this;
        var startOfMonth = moment().startOf('month').toDate();
        var endOfMonth = moment().endOf('month').toDate();
        var activityDateRange = !!qualityAdminState.timeFrame() ? qualityAdminState.timeFrame :
            ko.observableArray([startOfMonth.toISOString() + '/' + endOfMonth.toISOString()]);

        var checkImages = function(avatarImages){
            // if any image size has a "no_img" value then return false so the default image will show
            // the reason for returning false and not one with an image if it exists to to keep images consistent
            // in the template we care about the x96 size, for brevity sake
            return !_.include(avatarImages(), "no_img");
        };

        //
        // Public Attributes
        //

        self.userId = ko.observable();

        self.dateRangesView = self.subView(DateRangeEditorView, activityDateRange);

        self.isLoadingEvaluations = ko.observable(true);

        self.completedEvalGrid = {
            columns: COMPLETED_GRID_COLUMNS,
            rows: ko.observableArray([]),
            loading: self.isLoadingEvaluations
        };

        self.pendingEvalGrid = {
            columns: PENDING_GRID_COLUMNS,
            rows: ko.observableArray([]),
            loading: self.isLoadingEvaluations
        };

        self.hasCompletedEvaluations = self.computed(function () {
            return self.completedEvalGrid.rows().length > 0;
        });

        self.hasPendingEvaluations = self.computed(function () {
            return self.pendingEvalGrid.rows().length > 0;
        });

        self.agentActivityWidget = self.subView(AgentActivityWidget, activityDateRange, authService.principalId);

        self.canViewEvaluationDashboard = qualityAuth.canEditEvaluation() || qualityAuth.canEditScore();

        self.configure = function(config){
            self.userId(config.userId);
            self.user = ko.observable();
            self.userImages = ko.observable();
            userService.getUser(config.userId).then(function(user){
                self.user(user);
                if (user.avatarImages && user.avatarImages() && checkImages(user.avatarImages)) {
                    self.userImages(user.avatarImages());
                }
            });
            self.computed(function () {
                // Load the evaluations for the selected date range value in activityDateRange.

                self.isLoadingEvaluations(true);
                var times = activityDateRange()[0].split("/");
                var send_options = {
                    startTime: times[0],
                    endTime: times[1]
                };
                send_options.evaluatorUserId = self.userId();

                var completedEvalPromise = getAssignedEvaluations(_.assign(COMPLETED_EVALUATION_OPTIONS, send_options))
                    .then(function(queryResponse) {
                        populateGrid(self.completedEvalGrid, queryResponse.entities());
                    })
                    .catch(errorCallbackFactory.createCallback('evalatorDashboard.loadEvaluations.error'));

                var pendingEvalPromise = getAssignedEvaluations(_.assign(PENDING_EVALUATION_OPTIONS, send_options))
                    .then(function(queryResponse) {
                        populateGrid(self.pendingEvalGrid, queryResponse.entities());
                    })
                    .catch(errorCallbackFactory.createCallback('evalatorDashboard.loadEvaluations.error'));

                Promise.all([completedEvalPromise, pendingEvalPromise])
                    .finally(function () {
                        self.isLoadingEvaluations(false);
                    });
            });
        };


    });

    return EvaluatorDashboard;
});
