define([
    'core!lib/lodash',
    'core!lib/knockout',
    'core!viewModels/ViewModel',
    'core!services/navHelper',
    'core!services/qm/qualityAuth',

    'checkout/models/KeywordSet',
    'checkout/services/keywordService',
    'checkout/services/ErrorCodeMapper',
    'core!services/routeService'
], function (_,
             ko,
             ViewModel,
             navHelper,
             qualityAuth,

             KeywordSet,
             keywordService,
             ErrorCodeMapper,
             routeService) {
    'use strict';

    /**
     * The KeywordView class.
     * @class
     */
    var KeywordView = ViewModel.extend(function (keywordSet, keyword, addCallback, cancelCallback, updateCallback) {
        var self = this;
        self.isOpen = ko.observable(false);
        self.isNew = ko.observable(true);
        self.keywordSet = ko.observable(keywordSet);
        self.keyword = ko.observable(keyword);
        self.isLoading = ko.observable(false);
        self.saving = ko.observable(false);

        var validator = {
            validator: function (val) {
                return val && val.length > 0;
            },
            message: 'knockout.validation.required'
        };

        var numberValidator = {
            validator: function (val) {
                return !!val;
            },
            message: 'knockout.validation.required'
        };


        self.updating = ko.observable(false);
        if(keyword.phrase()) {
            self.updating(true);
        }

        self.wantsAlternateSpellings = ko.observable(self.keyword().alternateSpellings() && self.keyword().alternateSpellings().length > 0);

        self.computed(function(){
            var booleanValue = self.wantsAlternateSpellings();
            if (booleanValue && self.keyword().alternateSpellings() && self.keyword().alternateSpellings().length > 0){
                var newKeywords = _.map(self.keyword().alternateSpellings(), function(spelling){
                    return ko.isObservable(spelling) ? spelling : ko.observable(spelling);
                });
                self.keyword().alternateSpellings(newKeywords);
            } else if (booleanValue && (!self.keyword().alternateSpellings() || self.keyword().alternateSpellings().length === 0)){
                self.keyword().alternateSpellings([ko.observable("")]);
                self.keyword().alternateSpellings.isModified(false);
            } else if (!booleanValue){
                self.keyword().alternateSpellings(null);
            }
        });

        self.canRemoveKeywordAlternateSpelling = self.computed(function(){
            return self.keyword().alternateSpellings() && self.keyword().alternateSpellings().length > 1;
        });

        self.addAlternateSpelling = function(index){
            return function() {
                var alternateSpellings = self.keyword().alternateSpellings();
                alternateSpellings.splice(index + 1, 0, ko.observable(""));
                self.keyword().alternateSpellings(alternateSpellings);
                self.keyword().alternateSpellings.isModified(false);
            };
        };

        self.removeAlternateSpelling = function(index){
            return function() {
                var alternateSpellings = self.keyword().alternateSpellings();
                if (alternateSpellings.length > 1){
                    alternateSpellings.splice(index, 1);
                    self.keyword().alternateSpellings(alternateSpellings);
                }
            };
        };

        self.canAdd = qualityAuth.canAddKeywordSet ? qualityAuth.canAddKeywordSet() : false;
        self.canDelete = qualityAuth.canDeleteKeywordSet ? qualityAuth.canDeleteKeywordSet() : false;
        self.canEdit = qualityAuth.canEditKeywordSet ? qualityAuth.canEditKeywordSet() : false;
        self.canSave = ko.observable(self.canAdd || self.canEdit);

        self.isUniqueKeywordName = ko.observable(true);

        self.pageTitle = ko.observable('');
        routeService.breadcrumbsNavData({
            labelData: {
                updateKeywordSet: self
            }
        });

        self.keyword().phrase.extend({
            validation: [validator,
            {
                validator: function (val, param) {
                    return self.keywordSet().isUniqueKeywordPhrase(val, param);
                },
                message: 'quality.keywords.duplicatePhrase',
                params: self.keyword()
            }]
        });

        self.keyword().confidence.extend({
            validation: [numberValidator]
        });

        self.keyword().agentScoreModifier.extend({required: true});

        self.keyword().customerScoreModifier.extend({required: true});

        self.keyword().alternateSpellings.extend({
            validation: [{
                validator: function(val){
                    var badItems = _.filter(val, function(spelling){
                        return !ko.isObservable(spelling) || spelling().length === 0;
                    });
                    return !badItems || badItems.length === 0;
                },
                message: 'knockout.validation.required'
            }]
        });

        self.cancel = cancelCallback;


        function needsEvaluated(prop, propValue){
            if (prop === "phrase"){
                return ko.isObservable(propValue) && (!propValue() || propValue.isModified());
            }
            return ko.isObservable(propValue) && (propValue.isValid && !propValue.isValid());
        }

        self.addKeyword = function(keywordViewModel) {
            var keywordModel = keywordViewModel.keyword;
            // keywordObject is a NewClass but with properties that are Observables instead of strings
            var keywordObject = keywordViewModel.keyword();

            var valid = true;
            for(var prop in keywordModel()) {
                var propValue = keywordModel()[prop];
                if(needsEvaluated(prop, propValue)) {
                    propValue.isModified(true);
                    valid = valid && propValue.isValid();
                }
            }

            if(valid) {
                if(self.updating()) {
                    updateCallback(keywordObject);                
                } else {
                    addCallback(keywordViewModel.keyword());
                }            
            }
        };

        self.enableSave = self.computed(function() {
            return !self.saving() && self.canSave();
        });

    });

    return KeywordView;
});
