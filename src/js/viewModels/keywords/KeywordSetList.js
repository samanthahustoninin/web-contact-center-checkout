define([
    'core!lib/lodash',
    'core!lib/bluebird',
    'core!lib/knockout',
    'core!lib/base64',
    'checkout/models/KeywordSet',
    'core!viewModels/ViewModel',
    'checkout/services/keywordService',
    'core!services/loggingService',
    'core!services/routeService',
    'core!viewModels/widgets/dataGridCells/enumFilter',
    'core!services/qm/qualityAuth',
    'core!services/featureToggleService',
    'core!util/errorCallbackFactory'
], function (_, Promise, ko, base64, KeywordSet, ViewModel, keywordService, LOG, routeService, EnumFilter, qualityAuth, featureToggleService, errorCallbackFactory) {


    'use strict';

    /**
     * The KeywordsList class.
     * @class
     */
    var KeywordSetList = ViewModel.extend(function () {
        var self = this;

        var KEYWORDS_COLUMNS = ko.observableArray([
            {
                id: 'name',
                label: 'quality.keywords.keywordSetName'
            },
            {
                id: 'description',
                label: 'quality.keywords.keywordSet.description'
            },
            {
                id: 'queue',
                label: 'quality.keywords.keywordSet.queues.label'
            },
            {
                id: 'agentNames',
                label: 'quality.keywords.keywordSet.agents.label'
            }
        ]);


        self.gridOptions = ko.observable({
            pageCount: 1,
            pageNumber: 1,
            pageSize: 25
        });

        self.keywordSet = ko.observable();
        self.keywordSetGrid = ko.observable();
        self.newKeywordSetHref = '#checkout/admin/keywords/keywordsets/new';
        self.loading = ko.observable(true);

        self.canAdd = qualityAuth.canAddKeywordSet ? qualityAuth.canAddKeywordSet() : false;
        self.canDelete = qualityAuth.canDeleteKeywordSet ? qualityAuth.canDeleteKeywordSet() : false;
        self.canEdit = qualityAuth.canEditKeywordSet ? qualityAuth.canEditKeywordSet() : false;

        self.refreshGrid = function(incomingOptions) {
            var options = {};

            if (incomingOptions){
                if (incomingOptions.pageSize > self.gridOptions().pageSize){
                    options.pageNumber = 1;
                } else {
                    options.pageNumber = incomingOptions.pageNumber;
                }
                options.pageSize = incomingOptions.pageSize;
            }

            if (self.keywordSet()){
                options.name = self.keywordSet();
            }

            keywordService.getKeywordSets(options)
                .then(function (response) {
                    self.gridOptions({
                        pageCount: response.pageCount,
                        pageNumber: response.pageNumber,
                        pageSize: response.pageSize
                    });
                    var rows = _.map(response.entities, function(keywordSetData){
                        var keywordSetNameCell = {
                            label: keywordSetData.name,
                            href: '#checkout/admin/keywords/keywordsets/update/' + keywordSetData.id
                        };

                        var queue = !!keywordSetData.queue ? keywordSetData.queue.name : "";

                        var agentNames = (function(agents){
                            if (agents && agents.length > 0){
                                return _.map(agents, function(agent){
                                    return agent.displayName;
                                });
                            }
                            return [];
                        })(keywordSetData.agents);
                        return {
                            'id': keywordSetData.id,
                            'name': keywordSetNameCell,
                            'description': keywordSetData.description,
                            'queue': queue,
                            'agentNames': ko.observable(agentNames),
                            'selfUri': keywordSetData.selfUri
                        };
                    });
                    self.keywordSetGrid().rows(rows);
                }).catch(errorCallbackFactory.createCallback({
                    "namespace": "quality.keywords.keywordRequest"
                })).finally(function(){
                    self.loading(false);
                });
        };

        self.keywordSetGrid({
                columns: KEYWORDS_COLUMNS,
                rows: ko.observableArray([]),
                selectedRows: ko.observableArray([]),
                dataOptions: self.gridOptions,
                onDataOptionsChange: self.refreshGrid,
                loading: self.loading
            });
        self.refreshGrid();

        function performRefresh(){
            self.refreshGrid();
            self.keywordSetGrid().selectedRows([]);
        }

        self.deleteSelectedRows = function () {
            var options = {};
            self.loading(true);
            var selectedRows = self.keywordSetGrid().selectedRows();

            options.ids = _.map(selectedRows, function(row){
                return row.id;
            }).toString();

            keywordService.batchDeleteKeywordSets(options)
                .then(function(){ performRefresh(); })
                .catch(errorCallbackFactory.createCallback({
                    "namespace": 'quality.keywords.deleteError'
                }));
        };

        self.areRowsSelected = self.computed(function(){
            return self.keywordSetGrid() && self.keywordSetGrid().selectedRows().length > 0;
        });

        self.clearFields = function(){
            self.keywordSet("");
            self.refreshGrid();
        };

    });

    return KeywordSetList;
});