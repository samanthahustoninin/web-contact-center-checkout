define([
    'core!jquery',
    'core!lib/lodash',
    'core!lib/knockout',
    'core!lib/bluebird',
    'core!util/externalInclude',
    'core!viewModels/ViewModel',
    'core!services/navHelper',
    'core!services/qm/qualityAuth',

    'checkout/models/KeywordSet',
    'checkout/models/ParticipantPurposeTypes',
    'checkout/services/keywordService',
    'checkout/viewModels/keywords/KeywordView',
    'checkout/viewModels/widgets/InlineDialogView',
    'checkout/services/ErrorCodeMapper',
    'core!services/routeService',
    'core!util/errorCallbackFactory',
], function ($,
             _,
             ko,
             Promise,
             include,
             ViewModel,
             navHelper,
             qualityAuth,

             KeywordSet,
             ParticipantPurposeTypes,
             keywordService,
             KeywordView,
             InlineDialogView,
             ErrorCodeMapper,
             routeService,
             errorCallbackFactory
             ) {
    'use strict';

    /**
     * The KeywordSetView class.
     * @class
     */
    var KeywordSetView = ViewModel.extend(function () {
        var self = this;
        self.isOpen = ko.observable(false);
        self.isNew = ko.observable(true);
        self.keywordSet = ko.observable();
        self.keywordGrid = ko.observable();
        self.isLoading = ko.observable(true);
        self.attemptedSave = ko.observable(false);

        self.updateKeywordView = ko.observable();
        self.keywordSetAgents = ko.observableArray([]);

        self.canAdd = qualityAuth.canAddKeywordSet ? qualityAuth.canAddKeywordSet() : false;
        self.canDelete = qualityAuth.canDeleteKeywordSet ? qualityAuth.canDeleteKeywordSet() : false;
        self.canEdit = qualityAuth.canEditKeywordSet ? qualityAuth.canEditKeywordSet() : false;
    
        self.isUniqueKeywordSetName = ko.observable(true);
        self.participantPurposeTypes = _.toArray(ParticipantPurposeTypes.getParticipantPurposeTypes());

        self.pageTitle = ko.observable('');
        routeService.breadcrumbsNavData({
            labelData: {
                updateKeywordSet: self
            }
        });

        self.keywordSetListHref = function(){
            return "#checkout/admin/keywords/keywordSets";
        };

        self.addKeyword = function (keyword) {
            self.keywordSet().addKeyword(keyword);
            self.refreshGrid();
            self.keywordEditor.hide();
        };

        self.cancelKeywordEdit = function() {
            self.keywordEditor.hide();
        };

        self.updateKeyword = function(keyword) {
            self.keywordSet().updateKeyword(keyword);
            self.refreshGrid();
            self.keywordEditor.hide();
        };

        self.keywordEditor = self.subView(InlineDialogView, {
            title: 'quality.keywords.keywordEditor',
            template: 'checkout/keywords/keywordView',
            initViewModel: function () {
                var keywordView = self.subView(KeywordView, self.keywordSet(), 
                    self.keywordToEdit(), self.addKeyword, self.cancelKeywordEdit, self.updateKeyword);
                return keywordView;
            },
            onOpen: function () {
                if (self.keywordEditor.viewModel()) {
                    self.keywordEditor.viewModel().dispose();
                }
                self.keywordEditor.initViewModel();
            }
        });

        self.keywordToEdit = ko.observable();

        self.createKeywordEditor = function() {
            self.keywordToEdit(self.keywordSet().createKeyword());
            self.keywordEditor.open()();
        };

        self.openKeywordEditor = function (keywordRow) {
            // make a clone of this keyword so that editing the keyword in the popup
            // doesn't automatically update this one
            var clonedKeyword = self.keywordSet().cloneKeyword(keywordRow.keywordData);
            clonedKeyword.originalPhrase = ko.observable(keywordRow.keywordData.phrase());
            self.keywordToEdit(clonedKeyword);
            self.keywordEditor.open()();
        };


        var KEYWORDS_COLUMNS = ko.observableArray([
            {
                id: 'phrase',
                label: 'quality.keywords.phrase'
            },
            {
                id: 'confidence',
                label: 'quality.keywords.confidence'
            },
            {
                id: 'agentScoreModifier',
                label: 'quality.keywords.agentScoreModifier'
            },
            {
                id: 'customerScoreModifier',
                label: 'quality.keywords.customerScoreModifier'
            }
        ]);


        self.gridOptions = ko.observable({
            pageCount: 1,
            pageNumber: 1,
            pageSize: 25
        });

        // configure is called by the route controller, and will
        // setup the view model for either an update or a create
        self.configure = function (config) {

            var promise;
            if (config &&
                config.action === 'update' &&
                config.keywordSetId) {

                // Load an existing keywordSet
                self.isNew(false);
                self.canSave = self.canEdit;
                promise = keywordService.getKeywordSet(config.keywordSetId);

            } else if (config.action === 'create') {

                // configure the new keywordSet
                self.isNew(true);
                self.canSave = self.canAdd;
                promise = Promise.resolve(new KeywordSet());
            }

            promise
            .then(function (keywordSet) {
                self.keywordSet(keywordSet);
                self.pageTitle(self.keywordSet().name());
                self.keywordSetName = ko.computed({
                    read: function () {
                        return self.keywordSet().name();
                    },
                    write: function (newKeywordSetName) {
                        self.keywordSet().name(newKeywordSetName);
                        self.isUniqueKeywordSetName(true);
                    }
                }).extend({
                    validation: [{
                        validator: function (val) {
                            return val && val.length > 0;
                        },
                        message: 'knockout.validation.required'
                    },{
                        validator: function () {
                            return self.isUniqueKeywordSetName();
                        },
                        message: 'quality.keywords.duplicateKeywords'
                    }]
                });

                self.refreshGrid();
                self.initQueueView();
                self.initAgentsView();
            })
            .catch(errorCallbackFactory.createCallback({
                namespace: "quality.keywords.keywordFetch"
            }))
            .finally(function () {
                self.isLoading(false);
            });
        };

        self.hasAgents = self.computed(function(){
            return !!(self.keywordSetAgents() && self.keywordSetAgents().length > 0);
        });

        self.queuesView = ko.observable();
        self.initQueueView = function() {
            // var queueOptions = {
            //     queueValidation: {
            //         validation: [{
            //             validator: function (value) {
            //                 return !self.attemptedSave() || !!value || self.hasAgents();
            //             },
            //             message: 'knockout.validation.required'
            //         }]
            //     }
            // };
 //           self.queuesView(self.subView(QueueSelectorView, self.keywordSet().queue, queueOptions));
        };

        self.agentsView = ko.observable();
        self.initAgentsView = function() {
            self.keywordSetAgents(self.keywordSet().agents());
            // var agentOptions = {
            //     agentValidation: {
            //         validation: [{
            //             validator: function (value) {
            //                 return !self.attemptedSave() || (!!value && value.length > 0) || self.keywordSet().queue();
            //             },
            //             message: 'knockout.validation.required'
            //         }]
            //     }
            // };
//            self.agentsView(self.subView(AgentSelectorView, self.keywordSetAgents, agentOptions));
        };

        self.languageOptions = [
            {id: 'en-US', name: ko.i18n.renderString("quality.languageSelectorView.en-US")},
            {id: 'es-US', name: ko.i18n.renderString("quality.languageSelectorView.es-US")},
            {id: 'en-AU', name: ko.i18n.renderString("quality.languageSelectorView.en-AU")},
            {id: 'en-GB', name: ko.i18n.renderString("quality.languageSelectorView.en-GB")},
            {id: 'fr-CA', name: ko.i18n.renderString("quality.languageSelectorView.fr-CA")},
            {id: 'de-DE', name: ko.i18n.renderString("quality.languageSelectorView.de-DE")},
            {id: 'pt-BR', name: ko.i18n.renderString("quality.languageSelectorView.pt-BR")}
        ];

        self.selectedLanguage = ko.observable();

        // set up grid
        self.refreshGrid = function() {
            var keywords = self.keywordSet().keywords();
            var rows = _.map(keywords, function(keywordData) {
                if(keywordData) {
                    var keywordNameCell = {
                        label: keywordData.phrase
                    };
                    return {
                        'id': keywordData.id,
                        'phrase': keywordNameCell,
                        'confidence': keywordData.confidence,
                        'agentScoreModifier': keywordData.agentScoreModifier,
                        'customerScoreModifier': keywordData.customerScoreModifier,
                        'editKeyword': self.openKeywordEditor,
                        'keywordData': keywordData
                    };
                }
            });
            self.keywordGrid().rows(rows);
        };

        self.keywordGrid({
            columns: KEYWORDS_COLUMNS,
            rows: ko.observableArray([]),
            selectedRows: ko.observableArray([]),
            dataOptions: self.gridOptions,
            onDataOptionsChange: self.refreshGrid,
            loading: self.isLoading,
            openKeywordEditor: self.openKeywordEditor
        });

        self.isInvalidKeyword = self.computed(function(){
            return self.attemptedSave() && (!self.keywordSet().keywords() || self.keywordSet().keywords().length === 0 ||
                !self.queuesView().queues.isValid() || !self.agentsView().agents.isValid());
        });

        self.save = function() {
            var promise;
            self.attemptedSave(true);
            if (self.keywordSet() && !self.isInvalidKeyword()) {
                self.saving(true);
                var agents = ko.unwrap(self.keywordSetAgents);
                self.keywordSet().agents = _.map(agents, function (agent) {
                    return {
                        id: agent.id
                    };
                });

                if (self.isNew()) {
                    promise = keywordService.createKeywordSet(self.keywordSet());
                } else {
                    promise = keywordService.updateKeywordSet(self.keywordSet());
                }

                promise
                    .then(function() {
                        navHelper.navigateTo(self.keywordSetListHref());
                    })
                    .catch(function(error){
                        var httpStatus = error.type === 'http' ? error.details.status : undefined;
                        if(409 === httpStatus){
                            self.isUniqueKeywordSetName(false);
                        }else {
                            errorCallbackFactory.createCallback({
                                namespace: "quality.keywords.keywordSave",
                                logError: true
                            })(error);
                        }
                    })
                    .finally(function() {
                        self.saving(false);
                    });
            }
        };

        self.areRowsSelected = self.computed(function(){
            return self.keywordGrid() && self.keywordGrid().selectedRows().length > 0;
        });

        self.deleteSelectedRows = function () {
            var selectedRows = self.keywordGrid().selectedRows();

            _.each(selectedRows, function(row) {
                self.keywordSet().deleteKeyword(row.keywordData);
            });
            
            self.refreshGrid();
        };

        self.enableSave = self.computed(function() {
            if (!self.keywordSet()) {
                return false;
            }
            return !self.saving() && self.canAdd;
        });

        self.saving = ko.observable(false);
    });

    return KeywordSetView;
});
