define([
    'core!jquery',
    'core!lib/knockout',
    'core!lib/lodash',
    'core!viewModels/ViewModel'
], function (
    $,
    ko,
    _,
    ViewModel) {
    'use strict';


    /**
     * A viewModel to facilitate the presentation of another full template represented as a dialog container
     * within a parent view.  The viewModel/template within the dialog container is fully responsible
     * for it's own validation and behavior.  The inline dialog will show/hide the parent container
     * as well as provide hooks for onOpen/onClose events and initializing the child viewModel.
     *

     title
     template
     viewModel

     initViewModel
     onOpen
     onCancel


     *
     * # Required Arguments
     *
     * @param {String} title - A localization key for the title of the dialog
     * @param {String} template - A path to the template of the child viewModel.
     * @param {Object} viewModel - The child viewModel to be contained within the dialog container.
     *                             This parameter is not required if the optional initViewModel parameter is declared.
     *
     *
     * # Optional Arguments
     *
     * @param {Function} initViewModel - Will initialize the viewModel property for each inlineDialog initialization.
     *                                   This is useful when the viewModel requires further configuration after initialization.
     *                                   This can also be used optionally to re-initialize the viewModel property as needed.
     * @param {Function} onOpen - A hook that, if defined, is called when the inlineDialog.open method is called.
     * @param {Function} onCancel - A hook that, if defined, is called when the inlineDialog.cancel method is called.
     *
     *
     *
     * # Example
     *
     *     * //initialize an inlineDialog subView within your parent viewModel
     *   self.inlineCreateDncListView = self.subView(InlineDialog, {
     *       title: 'outbound.CampaignEditor.dialog.dncListTitle',
     *       template: 'templates/appMain/privilegedMain/outbound/dncLists/dncListEditor',
     *       initViewModel: function () {
     *           //this viewModel needs a config to tweak it for our inlineDialog usage
     *           var dncListView = self.subView(DncListEditor, {
     *               //... config for inlineDialog context
     *               }
     *           });
     *
     *           dncListView.configure({action: 'create'});
     *
     *           return dncListView;
     *       },
     *       onOpen: function() {
     *           //we want to re-initialize this viewModel on every dialog open, so dispose of any previous
     *           if(self.inlineCreateDncListView.viewModel()) {
     *               self.inlineCreateDncListView.viewModel().dispose();
     *           }
     *
     *           self.inlineCreateDncListView.initViewModel();
     *       }
     *   });
     *
     *
     * //on your parent template:
     * //include the inlineDialog template with a reference to the inlineDialog in your viewModel
     *   <!-- ko include: {
     *       name: 'templates/appMain/privilegedMain/outbound/common/inlineDialog',
     *       data: inlineCreateDncListView
     *   } -->
     *   <!-- /ko -->
     *
     * //and reference the .open() method on your inlineDialog - in this example, using the fixedOptions on the combobox
     *   <input class="form-control" id="cd-dnc-list-select" type="hidden" data-bind="
     *       combobox: {
     *           search: searchDncLists,
     *           value: campaign().dncLists,
     *           fixedOptions: {
     *               name: 'outbound.CampaignEditor.dialog.dncListTitle',
     *               click: inlineCreateDncListView.open()
     *           }
     *       }
     *   ">
     */
    return ViewModel.extend(function(config) {
        var self = this;

        //bound to the displaying of the dialog template
        self.showInlineDialog = ko.observable(false);
        self.cancellable = ko.observable(config.cancellable !== false);
        self.show = function () {
            self.showInlineDialog(true);
        };
        self.hide = function () {
            self.showInlineDialog(false);
        };

        self.title = config.title;
        self.template = ko.observable(config.template);
        self.viewModel = ko.observable(config.viewModel);
        self.initViewModel = function () {
            if(config.initViewModel) {
                self.viewModel(config.initViewModel());
            }
        };

        self.onOpen = config.onOpen;
        self.onCancel = config.onCancel;

        self.open = ko.observable(function () {
            if (!self.showInlineDialog()) {
                if(self.onOpen) {
                    self.onOpen();
                }
                self.show();
            }
        });

        self.cancel = ko.observable(function () {
            if (self.showInlineDialog()) {
                if(self.onCancel){
                    self.onCancel();
                }
                self.hide();
            }
        });
    });
});
