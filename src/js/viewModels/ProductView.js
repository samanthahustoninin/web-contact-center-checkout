define([
    'core!jquery',
    'core!lib/lodash',
    'core!lib/knockout',
    'core!lib/bluebird',
    'core!util/externalInclude',
    'core!viewModels/ViewModel',
    'core!services/navHelper',
    'core!services/qm/qualityAuth',

    'checkout/models/Product',
    'checkout/models/ParticipantPurposeTypes',
    'checkout/services/productService',
    'checkout/viewModels/keywords/KeywordView',
    'checkout/viewModels/widgets/InlineDialogView',
    'checkout/services/ErrorCodeMapper',
    'core!services/routeService',
    'core!util/errorCallbackFactory',
], function ($,
             _,
             ko,
             Promise,
             include,
             ViewModel,
             navHelper,
             qualityAuth,

             Product,
             ParticipantPurposeTypes,
             productService,
             KeywordView,
             InlineDialogView,
             ErrorCodeMapper,
             routeService,
             errorCallbackFactory
             ) {
    'use strict';

    /**
     * The KeywordSetView class.
     * @class
     */
    var ProductView = ViewModel.extend(function () {
        var self = this;
        self.isOpen = ko.observable(false);
        self.isNew = ko.observable(true);
        self.product = ko.observable();
        // self.keywordGrid = ko.observable();
        self.isLoading = ko.observable(true);
        self.attemptedSave = ko.observable(false);

        self.updateKeywordView = ko.observable();
        // self.keywordSetAgents = ko.observableArray([]);

        self.canAdd = qualityAuth.canAddKeywordSet ? qualityAuth.canAddKeywordSet() : false;
        self.canDelete = qualityAuth.canDeleteKeywordSet ? qualityAuth.canDeleteKeywordSet() : false;
        self.canEdit = qualityAuth.canEditKeywordSet ? qualityAuth.canEditKeywordSet() : false;
    
        self.isUniqueProductName = ko.observable(true);
        self.isUniqueProductId = ko.observable(true);
        self.participantPurposeTypes = _.toArray(ParticipantPurposeTypes.getParticipantPurposeTypes());

        self.pageTitle = ko.observable('');
        routeService.breadcrumbsNavData({
            labelData: {
                updateKeywordSet: self
            }
        });

        self.productListHref = function(){
            return "#checkout/admin/products";
        };


        // configure is called by the route controller, and will
        // setup the view model for either an update or a create
        self.configure = function (config) {

            var promise;
            if (config &&
                config.action === 'update' &&
                config.productId) {

                // Load an existing keywordSet
                self.isNew(false);
                self.canSave = self.canEdit;
                promise = productService.getProduct(config.productId);

            } else if (config.action === 'create') {

                // configure the new keywordSet
                self.isNew(true);
                self.canSave = self.canAdd;
                promise = Promise.resolve(new Product());
            }

            promise
            .then(function (product) {
                self.product(product);
                self.pageTitle(self.product().name);
                // look for the default page title
                self.productName = ko.computed({
                    read: function () {
                        return self.product().name;
                    },
                    write: function (newProductName) {
                        self.product().name(newProductName);
                        self.isUniqueProductName(true);
                    }
                });
                // .extend({
                //     validation: [{
                //         validator: function (val) {
                //             return val && val.length > 0;
                //         },
                //         message: 'knockout.validation.required'
                //     },{
                //         validator: function () {
                //             return self.isUniqueProductName();
                //         },
                //         message: 'checkout.products.duplicateProductName'
                //     }]
                // });


                //validation for product ID

                self.productId = ko.computed({
                    read: function () {
                        return self.product().id;
                    },
                    write: function (newProductId) {
                        self.product().id(newProductId);
                        self.isUniqueProductId(true);
                    }
                });
                // .extend({
                //     validation: [{
                //         validator: function (val) {
                //             return val && val.length > 0;
                //         },
                //         message: 'knockout.validation.required'
                //     },{
                //         validator: function () {
                //             return self.isUniqueProductId();
                //         },
                //         message: 'checkout.products.duplicateProductId'
                //     }]
                // });

                // self.refreshGrid();
                // self.initQueueView();
                // self.initAgentsView();
            })
            .catch(errorCallbackFactory.createCallback({
                namespace: "checkout.products.productFetch"
            }))
            .finally(function () {
                self.isLoading(false);
            });
        };

        window.ko = ko;
        self.save = function() {
            var promise;
            self.attemptedSave(true);
            if (self.product()) {
                self.saving(true);

                if (self.isNew()) {
                    promise = productService.createProduct(self.product());
                } else {
                    promise = productService.updateProduct(self.product());
                }

                promise
                    .then(function() {
                        navHelper.navigateTo(self.productListHref());
                    })
                    .catch(function(error){
                        var httpStatus = error.type === 'http' ? error.details.status : undefined;
                        if(409 === httpStatus){
                            self.isUniqueProductName(false);
                        }else {
                            errorCallbackFactory.createCallback({
                                namespace: "checkout.products.productSave",
                                logError: true
                            })(error);
                        }
                    })
                    .finally(function() {
                        self.saving(false);
                    });
            }
        };

        self.enableSave = self.computed(function() {
            if (!self.product()) {
                return false;
            }
            return !self.saving() && self.canAdd;
        });

        self.saving = ko.observable(false);
    });

    return ProductView;
});
