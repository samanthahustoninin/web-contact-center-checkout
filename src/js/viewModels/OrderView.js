define([
    'core!jquery',
    'core!lib/lodash',
    'core!lib/knockout',
    'core!lib/bluebird',
    'core!util/externalInclude',
    'core!viewModels/ViewModel',
    'core!services/navHelper',
    'core!services/qm/qualityAuth',

    'checkout/models/Order',
    'checkout/models/ParticipantPurposeTypes',
    'checkout/services/orderService',
    'checkout/viewModels/keywords/KeywordView',
    'checkout/viewModels/widgets/InlineDialogView',
    'checkout/services/ErrorCodeMapper',
    'core!services/routeService',
    'core!util/errorCallbackFactory',
], function ($,
             _,
             ko,
             Promise,
             include,
             ViewModel,
             navHelper,
             qualityAuth,

             Order,
             ParticipantPurposeTypes,
             orderService,
             KeywordView,
             InlineDialogView,
             ErrorCodeMapper,
             routeService,
             errorCallbackFactory
             ) {
    'use strict';

    /**
     * The KeywordSetView class.
     * @class
     */
    var OrderView = ViewModel.extend(function () {
        var self = this;
        self.isOpen = ko.observable(false);
        self.isNew = ko.observable(true);
        self.order = ko.observable();
        self.isLoading = ko.observable(true);
        self.attemptedSave = ko.observable(false);
        self.itemsList = ko.observable("");

        self.updateKeywordView = ko.observable();

        self.canAdd = qualityAuth.canAddKeywordSet ? qualityAuth.canAddKeywordSet() : false;
        self.canDelete = qualityAuth.canDeleteKeywordSet ? qualityAuth.canDeleteKeywordSet() : false;
        self.canEdit = qualityAuth.canEditKeywordSet ? qualityAuth.canEditKeywordSet() : false;
    
        self.isUniqueProductName = ko.observable(true);
        self.isUniqueProductId = ko.observable(true);
        self.participantPurposeTypes = _.toArray(ParticipantPurposeTypes.getParticipantPurposeTypes());

        self.pageTitle = ko.observable('');
        routeService.breadcrumbsNavData({
            labelData: {
                updateKeywordSet: self
            }
        });

        self.orderListHref = function(){
            return "#checkout/admin/orders";
        };

        self.statusOptions = [
            {id: 'processing', name: ko.i18n.renderString("checkout.orderStatus.processing")},
            {id: 'shipped', name: ko.i18n.renderString("checkout.orderStatus.shipped")},
            {id: 'cancelled', name: ko.i18n.renderString("checkout.orderStatus.cancelled")},
            {id: 'ordered', name: ko.i18n.renderString("checkout.orderStatus.ordered")}
        ];

        self.selectedStatus = ko.observable();


        // configure is called by the route controller, and will
        // setup the view model for either an update or a create
        self.configure = function (config) {

            var promise;

            promise = orderService.getOrder(config.orderId);

            promise
            .then(function (order) {
                self.order(order);
                self.selectedStatus(order.status);
                self.pageTitle(self.order().customerName);
                self.order().items().forEach(function(item){
                    self.itemsList(self.itemsList() + "ItemId: " + item.itemId() + ", " + "Quantity: " + item.qty() + ", " + "Price: " + item.pricePaid() + "\n");
                });
            })
            .catch(errorCallbackFactory.createCallback({
                namespace: "checkout.orders.orderFetch"
            }))
            .finally(function () {
                self.isLoading(false);
            });
        };

        window.ko = ko;
        self.save = function() {
            var promise;
            self.attemptedSave(true);
            if (self.order()) {
                self.saving(true);

                promise = orderService.updateOrder(self.order());
                promise
                    .then(function() {
                        navHelper.navigateTo(self.orderListHref());
                    })
                    .catch(function(error){
                        var httpStatus = error.type === 'http' ? error.details.status : undefined;
                        if(409 === httpStatus){
                            self.isUniqueProductName(false);
                        }else {
                            errorCallbackFactory.createCallback({
                                namespace: "checkout.orders.orderSave",
                                logError: true
                            })(error);
                        }
                    })
                    .finally(function() {
                        self.saving(false);
                    });
            }
        };

        self.enableSave = self.computed(function() {
            if (!self.order()) {
                return false;
            }
            return !self.saving() && self.canAdd;
        });

        self.saving = ko.observable(false);
    });

    return OrderView;
});
