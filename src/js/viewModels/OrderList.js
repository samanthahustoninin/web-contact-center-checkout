define([
    'core!lib/lodash',
    'core!lib/bluebird',
    'core!lib/knockout',
    'core!lib/base64',
    'checkout/models/Order',
    'core!viewModels/ViewModel',
    'checkout/services/orderService',
    'core!services/loggingService',
    'core!services/routeService',
    'core!viewModels/widgets/dataGridCells/enumFilter',
    'core!services/qm/qualityAuth',
    'core!services/featureToggleService',
    'core!util/errorCallbackFactory'
], function (_, Promise, ko, base64, Order, ViewModel, orderService, LOG, routeService, EnumFilter, qualityAuth, featureToggleService, errorCallbackFactory) {


    'use strict';

    /**
     * The KeywordsList class.
     * @class
     */
    var OrderList = ViewModel.extend(function () {
        var self = this;

        var ORDER_COLUMNS = ko.observableArray([
            {
                id: 'id',
                label: 'checkout.orders.orderId'
            },
            {
                id: 'name',
                label: 'checkout.orders.customerName'
            },
            {
                id: 'address',
                label: 'checkout.orders.shippingAddress'
            },
            {
                id: 'status',
                label: 'checkout.orders.status'
            },
            {
                id: 'total',
                label: 'checkout.orders.total'
            }
        ]);


        self.gridOptions = ko.observable({
            pageCount: 1,
            pageNumber: 1,
            pageSize: 25
        });

        self.order = ko.observable(); // This is actually the data that is retrieved from the service
        self.orderGrid = ko.observable();  // not sure what this is
        self.newOrderHref = '#checkout/admin/orders/new'; // create a new product page
        self.loading = ko.observable(true);

        /**
        * For future code ppl,
        * we stole this code from the quality component
        * We didn't want to deal with permission stuff,
        * So keywords = products
        * AND evaluators = orders
        * Just create an admin user with all quality perms and you should be fine
        */
        self.canAdd = qualityAuth.canAddKeywordSet ? qualityAuth.canAddKeywordSet() : false;
        self.canDelete = qualityAuth.canDeleteKeywordSet ? qualityAuth.canDeleteKeywordSet() : false;
        self.canEdit = qualityAuth.canEditKeywordSet ? qualityAuth.canEditKeywordSet() : false;

        self.refreshGrid = function() {
            // var options = {};

            // if (incomingOptions){
            //     if (incomingOptions.pageSize > self.gridOptions().pageSize){
            //         options.pageNumber = 1;
            //     } else {
            //         options.pageNumber = incomingOptions.pageNumber;
            //     }
            //     options.pageSize = incomingOptions.pageSize;
            // }

            // if (self.product()){
            //     options.name = self.product();
            // }

            orderService.getOrders()
                .then(function (response) {
                    self.gridOptions({
                        pageCount: response.pageCount,
                        pageNumber: response.pageNumber,
                        pageSize: response.pageSize
                    });
                    var rows = _.map(response.entities, function(orderData){
                        var customerIdCell = {
                            label: orderData.id,
                            href: '#checkout/admin/orders/update/' + orderData.id()
                        };
                        
                        return {
                            'id': customerIdCell,
                            'name': orderData.customerName,
                            'description': orderData.desc,
                            'address': orderData.shippingAddress,
                            'status': orderData.status,
                            'total': orderData.total,
                            'selfUri': orderData.selfUri
                        };
                    });
                    self.orderGrid().rows(rows);
                }).catch(errorCallbackFactory.createCallback({
                    "namespace": "checkout.products.productRequest"
                })).finally(function(){
                    self.loading(false);
                });
        };

        self.orderGrid({
                columns: ORDER_COLUMNS,
                rows: ko.observableArray([]),
                selectedRows: ko.observableArray([]),
                dataOptions: self.gridOptions,
                onDataOptionsChange: self.refreshGrid,
                loading: self.loading
            });
        self.refreshGrid();

        function performRefresh(){
            self.refreshGrid();
            self.orderGrid().selectedRows([]);
        }

        self.deleteSelectedRows = function () {
            self.loading(true);
            var selectedRows = self.orderGrid().selectedRows();
            var id = selectedRows[0].id();

            orderService.deleteOrders(id)
            .then(function(){ performRefresh(); })
            .catch(errorCallbackFactory.createCallback({
                "namespace": 'checkout.orders.deleteError'
            }));
        };

        self.areRowsSelected = self.computed(function(){
            return self.orderGrid() && self.orderGrid().selectedRows().length > 0;
        });

        self.clearFields = function(){
            self.order("");
            self.refreshGrid();
        };

    });

    return OrderList;
});