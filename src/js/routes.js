define([
    'core!lib/base64',
    'core!services/navHelper',
    'core!services/authService',
    'core!services/featureToggleService',
    
    'checkout/siteMap',
    'checkout/routePermissions'
], function (base64, navHelper, authService, featureToggleService, siteMap, routePermissions) {
    'use strict';

    // Maps the authorization(s) allowed to view a given path.
    var authorizationConstraints = [
        {
            path: 'checkout/admin/policies/.*/new',
            authorizations: routePermissions.RETENTION_POLICY_NEW
        },
        {
            path: 'checkout/admin/policies/.*/update/.*$',
            authorizations: routePermissions.RETENTION_POLICY_UPDATE
        },
        {
            path: 'checkout/admin/policies/.*$',
            authorizations: routePermissions.RETENTION_POLICIES
        },
        {
            path: 'checkout/admin/encryptionKeys',
            authorizations: routePermissions.ENCRYPTION_KEYS
        },
        {
            path: 'checkout/admin/orphanRecordings',
            authorizations: routePermissions.ORPHAN_RECORDINGS
        },
        {
            path: 'checkout/admin/evaluators',
            authorizations: routePermissions.EVALUATORS
        },
        {
            path: 'checkout/admin/evaluators/.*$',
            authorizations: routePermissions.EVALUATORS
        },
        {
            path: 'checkout/admin/keywords/keywordSets',
            authorizations: routePermissions.KEYWORD_SETS
        },
        {
            path: 'checkout/admin/keywords/keywordSets/new',
            authorizations: routePermissions.KEYWORD_SET_NEW
        },
        {
            path: 'checkout/admin/keywords/keywordSets/update/.*$',
            authorizations: routePermissions.KEYWORD_SET_UPDATE
        },         
        {
            path: 'checkout/admin/recordingManagement',
            authorizations: routePermissions.RECORDING_MANAGEMENT
        }
    ];

    // Routes owned by this component.
    // Paths will be validated to begin with the component
    // name.
    var routes = {

        'checkout': {
            pageInfo: siteMap.quality,
            handler: function () {
                if (authService.hasAuthorization({any : routePermissions.EVALUATORS})){
                    navHelper.navigateTo('#checkout/admin/products');
                }
            }
        },

        // ===============================================================================
        // Quality  Policies
        // ===============================================================================

        // ==============================================================================
        // Quality Evaluator
        // ==============================================================================
        'checkout/admin/evaluators': {
            pageInfo: siteMap.checkout.evaluatorList,
            handler: function(ctx){
                ctx.subViews.setActiveView('evaluatorList');
            }
        },

        'checkout/admin/evaluators/:evaluatorId': {
            pageInfo: siteMap.checkout.evaluatorList.evaluatorView,
            handler: function (ctx, sammy) {
                ctx.subViews.setActiveView('evaluatorView')
                    .configure({userId: sammy.params.evaluatorId});
            }
        },

        // ===============================================================================
        // Quality Keywords
        // ===============================================================================
        'checkout/admin/keywords/keywordSets': {
            pageInfo: siteMap.checkout.keywordsList,
            handler: function (ctx) {
                ctx
                    .subViews.setActiveView('keywordSetList');
            }
        },

        'checkout/admin/keywords/keywordsets/new': {
            pageInfo: siteMap.checkout.keywordsList.createKeywordSet,
            handler: function (ctx) {
                ctx
                    .subViews.setActiveView('keywordSetView')
                    .configure({ action: 'create' });
            }
        },

        'checkout/admin/keywords/keywordsets/update/:keywordSetId': {
            pageInfo: siteMap.checkout.keywordsList.updateKeywordSet,
            handler: function (ctx, sammy) {
                ctx
                    .subViews.setActiveView('keywordSetView')
                    .configure({ action: 'update',
                        keywordSetId: sammy.params.keywordSetId });
            }
        },
        
        // ==============================================================================
        // Product
        // ==============================================================================    

        'checkout/admin/products': {
            pageInfo: siteMap.checkout.productList,
            handler: function (ctx) {
                ctx
                    .subViews.setActiveView('productList');
            }
        },
        'checkout/admin/products/new': {
            pageInfo: siteMap.checkout.productList.createProduct,
            handler: function (ctx) {
                ctx
                    .subViews.setActiveView('productView')
                    .configure({ action: 'create' });
            }
        },

        'checkout/admin/products/update/:productId': {
            pageInfo: siteMap.checkout.productList.updateProduct,
            handler: function (ctx, sammy) {
                ctx
                    .subViews.setActiveView('productView')
                    .configure({ action: 'update',
                        productId: sammy.params.productId });
            }
        },

        // ==============================================================================
        // Order
        // ==============================================================================    

        
        'checkout/admin/orders': {
            pageInfo: siteMap.checkout.orderList,
            handler: function (ctx) {
                ctx
                    .subViews.setActiveView('orderList');
            }
        },

        
        'checkout/admin/orders/update/:orderId': {
            pageInfo: siteMap.checkout.orderList.updateOrder,
            handler: function (ctx, sammy) {
                ctx
                    .subViews.setActiveView('orderView')
                    .configure({ action: 'update',
                        orderId: sammy.params.orderId });
            }
        }

    };

    // Return aggregated route details to be registered with the core application
    return {
        routes: routes,
        routeAuthorizations: authorizationConstraints
    };
});
