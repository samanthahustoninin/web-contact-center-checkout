/**
 * A dummy requirejs plugin. Only used to assist with the build.
 */
define(function () {
    'use strict';
    return {
        load: function (name, req, onload) {
            onload({});
        }
    };
});
