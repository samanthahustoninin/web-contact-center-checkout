define(['core!lib/knockout'], function(ko) {
    "use strict";

    ko.extenders.revertible = function(target, option) {
        if (option) {
            var revertVal = target();
            target.revert = function() {
                target(revertVal);
            };
            target.updateRevertValue = function() {
                revertVal = target();
            };
            target.revertValue = function() {
                return revertVal;
            };
        }
        return target;
    };
});