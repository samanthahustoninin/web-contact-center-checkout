define([
    'core!lib/knockout',
    'core!lib/lodash',
    'core!moment',
    'core!bindingHandlers/util/spinnerUtil',
    'core!extensions/knockoutExtenders/numeric',
    'core!bindingHandlers/include'
], function(ko, _, moment, spinnerUtil) {
    "use strict";

    /**
     *  integer Picker
     *
     * @description: creates a straight up integer incrementer and decrementer
     *
     * REQUIRES:
     *
     * value (observable) - just an integer
     * 
     * OPTIONALS:
     * 
     * max - integer, max value configurable
     * min - integer, min value configurable
     *
     * You could do more with this. I am just trying to keep it really basic for the time being.
     */
    ko.bindingHandlers.integerPicker = {
        init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
            var self = this;
            var bindingData = valueAccessor();
            var item_template = bindingData.template ? bindingData.template : 'checkout/integerPicker_days';
            var templateData = {};
            var tempHolder = {};

            tempHolder.item = bindingData.value.extend({
                numeric: {
                    precision: 0,
                    min: _.isInteger(ko.unwrap(bindingData.min)) ? bindingData.min : 0,
                    max: _.isInteger(ko.unwrap(bindingData.max)) ? bindingData.max : 999999
                }
            });

            if (!_.isFunction(bindingData.value.revert)) {
                bindingData.value.extend({revertible: true});
            }
            
            templateData.item = tempHolder.item;
            
            templateData.displayReset = ko.computed(function() {
               return bindingData.value() !== bindingData.value.revertValue(); 
            }, null, { disposeWhenNodeIsRemoved: element });
            
            templateData.click_reset = function() {
                bindingData.value.revert();
            };

            // merge the templateData set above with the click handlers from the spinner utility
            _.merge(templateData, spinnerUtil.handleClickDirections(tempHolder));

            var includeValueAccessor = ko.observable({
                name: item_template,
                data: templateData
            });

            ko.bindingHandlers.include.init.call(self, element, includeValueAccessor, allBindings, viewModel, bindingContext);
            ko.bindingHandlers.include.update.call(self, element, includeValueAccessor, allBindings, viewModel, bindingContext);
        }
    };
});
