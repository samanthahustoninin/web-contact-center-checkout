define([
], function () {
    'use strict';

    /**
     * A simple structured object that maps to the
     * component heirarchy. Used to generate breadcrumbs
     * and will be added to the global site map.
     */
    return {
        checkout: {
            keywordsList: {
                createKeywordSet: {},
                updateKeywordSet: {}
            },
            evaluatorList: {
                evaluatorView: {}
            },
            productList: {
                createProduct: {},
                updateProduct: {}
            },
            orderList: {
                updateOrder: {}
            }
        }
    };
});
