define([
    'core!lib/knockout',
    'core!lib/lodash',
    'core!util/ObservableModel',
    'core!models/User',
    // No reference needed
    'core!extensions/i18n/knockout.i18n',
    'core!extensions/knockout.validation.extender'
], function (ko, _, ObservableModel, User) {
    'use strict';

    /**
     * @constructor
     * @this Call
     * Observable representation of a keyword spotter.
     * @return {Keyword} a keyword spotter
     */
    var Keyword = ObservableModel.extend({
            defaults: {
                phrase: null,
                confidence: 50,
                agentScoreModifier: 0,
                customerScoreModifier: 0,
                alternateSpellings: null
            }
        });


    /**
     * @constructor
     * @this Call
     * Observable representation of a keyword spotter.
     * @return {KeywordSet} a keyword spotter
     */
    var KeywordSet = ObservableModel.extend({
            modelProperties: {
                keywords: Keyword,
                agents: User
            },
            defaults: {
                id: '',
                organizationId: '',
                queue: null,
                agents: [],
                language: null,
                name: '',
                description: '',
                participantPurposes:  null,
                keywords: []
            }
        }, function() {
            var self = this;

        self.participantPurposes.extend(
            {
                validation: [{
                    validator: function (val) {
                        return val && val.length > 0;
                    },
                    message: 'knockout.validation.required'
                }]
            }
        );

            self.addKeyword = function(keyword) {
                self.keywords.push(keyword);
            };

            self.createKeyword = function() {
                return new Keyword();
            };

            self.cloneKeyword = function(keyword) {
                var clonedKeyword = new Keyword();
                clonedKeyword.phrase(keyword.phrase());
                clonedKeyword.confidence(keyword.confidence());
                clonedKeyword.agentScoreModifier(keyword.agentScoreModifier());
                clonedKeyword.customerScoreModifier(keyword.customerScoreModifier());
                clonedKeyword.alternateSpellings(keyword.alternateSpellings());
                return clonedKeyword;
            };

            self.updateKeyword = function(keyword) {
                var existingKeyword = ko.utils.arrayFirst(self.keywords(), function (keywordInArray) {
                    return keywordInArray.phrase() == keyword.originalPhrase();
                });
                
                if(existingKeyword) {
                    self.keywords.replace(existingKeyword, keyword);
                }
            };

            self.deleteKeyword = function (keyword) {
                self.keywords.remove(keyword);
            };

            self.isUniqueKeywordPhrase = function (keywordValue, keyword) {
                var existingKeyword = ko.utils.arrayFirst(self.keywords(), function (keywordInArray) {
                    return keyword && keyword.phrase && keywordInArray.phrase() == keyword.phrase();
                });
                return !existingKeyword;
            };
        });

    return KeywordSet;
});
