define([
    'core!lib/knockout',
    'core!lib/lodash',
    'core!util/ObservableModel',
    'checkout/models/MediaPolicy',
    // No reference needed
    'core!extensions/i18n/knockout.i18n',
    'core!extensions/knockout.validation.extender'
], function (ko, _, ObservableModel, MediaPolicy) {
    'use strict';

    /**
     * @constructor
     * @this Call
     * Observable representation of a policy.
     * @return {Policy} a policy
     */
    var Policy = ObservableModel.extend({
            modelProperties: {
                actions: ObservableModel.extend({
                    modelProperties: {
                        /* These are listed because they are arrays of objects, and each
                           object needs to be constructed as an ObservableModel */
                        assignEvaluations: ObservableModel,
                        assignMeteredEvaluations: ObservableModel,
                        assignCalibrations: ObservableModel
                    }
                }),
                conditions: ObservableModel,
                mediaPolicies: ObservableModel.extend({
                    modelProperties: {
                        callPolicy: MediaPolicy,
                        chatPolicy: MediaPolicy,
                        emailPolicy: MediaPolicy
                    }
                })
            },
            defaults: {
                name: '',
                enabled: true,
                type: undefined,
                description: '',
                conditions: {},
                actions: {},
                mediaPolicies: {}
            }
        });

    return Policy;
});
