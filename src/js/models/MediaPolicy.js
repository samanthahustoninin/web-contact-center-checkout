define([
    'core!util/ObservableModel',
], function (ObservableModel) {
    'use strict';

    /**
     * @constructor
     * @this Call
     * Observable representation of a policy.
     * @return {Policy} a policy
     */
    var MediaPolicy = ObservableModel.extend({
        modelProperties: {
            actions: ObservableModel.extend({
                modelProperties: {
                    /* These are listed because they are arrays of objects, and each
                       object needs to be constructed as an ObservableModel */
                    assignEvaluations: ObservableModel,
                    assignMeteredEvaluations: ObservableModel,
                    assignCalibrations: ObservableModel
                }
            }),
            conditions: ObservableModel,
        },
        defaults: {
            actions: {},
            conditions: {}
        }
    });

    return MediaPolicy;
});