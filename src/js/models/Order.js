define([
    'core!lib/knockout',
    'core!lib/lodash',
    'core!util/ObservableModel',
    // No reference needed
    'core!extensions/i18n/knockout.i18n',
    'core!extensions/knockout.validation.extender'
], function (ko, _, ObservableModel) {
    'use strict';

/**
     * @constructor
     * @this Call
     * Observable representation of an item
     * @return {Keyword} a keyword spotter
     */
    var Item = ObservableModel.extend({
            defaults: {
                qty: 0,
                pricePaid: 0,
                itemId: 0
            }
        });

    /**
     * @constructor
     * @this Call
     * Observable representation of a Order.
     * @return {Order} a Order
     */
    var Order = ObservableModel.extend({
        modelProperties: {
            items: Item
        },
        defaults: {
            id: null,
            status: null,
            total:0,
            shippingAddress: null,
            customerName: null,
            email: null
        }  
    }, function() {
        var self = this;
        self.id = ko.observable(self.id);
    });

    return Order;
});