define([
    'core!lib/knockout',
    'core!lib/lodash',
    'core!util/ObservableModel',
    // No reference needed
    'core!extensions/i18n/knockout.i18n',
    'core!extensions/knockout.validation.extender'
], function (ko, _, ObservableModel) {
    'use strict';

    /**
     * @constructor
     * @this Call
     * Observable representation of a product.
     * @return {product} a product
     */
    var Product = ObservableModel.extend({
            defaults: {
                id: null,
                name: null,
                desc: null,
                price: 0,
                tag: null
            }  
        }, function() {
            var self = this;
            self.id = ko.observable(self.id);
        });

    return Product;
});