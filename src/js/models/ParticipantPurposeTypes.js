define([
    'core!lib/knockout',
    'core!lib/lodash'
], function (
    ko,
    _
) {
    'use strict';


    var PARTICIPANT_PURPOSE_TYPES = {
        CUSTOMER: {
            id: 'CUSTOMER'
        },
        AGENT: {
            id: 'AGENT'
        }
    };

    var baseNameKey = 'quality.keywords.participantPurposetypes.';

    return {
        getParticipantPurposeTypes: function () {
            _.forOwn(PARTICIPANT_PURPOSE_TYPES, function (participantPurposeType) {
                var key = baseNameKey + participantPurposeType.id;
                participantPurposeType.name = ko.i18n.renderString(key);
            });
            return PARTICIPANT_PURPOSE_TYPES;
        },
        getUnlocalizedParticipantPurposeTypes: function () {
            return PARTICIPANT_PURPOSE_TYPES;
        }
    };
});
